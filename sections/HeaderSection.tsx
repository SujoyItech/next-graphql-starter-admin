import Link from "next/link";
import {useStaffQuery} from "../src/graphql/generated";
import Image from "next/image";
import brandLogo from "../public/assets/images/logo.png"
import defaultAvatar from "../public/assets/images/user.svg"
import {useEffect, useState} from "react";
import {useAuth} from "../src/contexts/AuthProvider"
import Router from "next/router";
import {AddToastAlert, ShowToastAlert} from "../src/Services/ToastAlertService";
import {useToasts} from "react-toast-notifications";

export default function HeaderSection(){
    const [me,setMe] = useState<any>(undefined);
    const {signOut} = useAuth();
    const {data,isSuccess} = useStaffQuery()
    const { addToast } = useToasts();
    ShowToastAlert();

    useEffect(()=>{
        if (isSuccess){
            setMe(data?data.staff : undefined);
        }
    },[data])

    const logout = async (e:any)=>{
        try {
            e.preventDefault();
            await signOut();
            AddToastAlert("Logout successful!","success");
            Router.push('/');
        }catch (error){
            addToast("Logout failed!",{appearance: "error"})
        }
    }
    return(
        <>
            <header className="dashboard-header-area">
                <div className="container-fluid">
                    <div className="header-wrap">
                        <div className="row align-items-center">
                            <div className="col-6">
                                <Link href="/">
                                    <a className="brand-logo"><Image src={brandLogo} alt="logo"/></a>
                                </Link>

                            </div>
                            <div className="col-6">
                                <div className="header-right text-right">
                                    <div className="user-area">
                                        <a className="profile-btn"><Image src={me && me.avatar ? me.avatar : defaultAvatar} alt="user"/> <span>{me && me.name}</span></a>
                                        <ul className="sub-menu-item">
                                            <li><Link href="/profile"><a><i className="fas fa-user"></i> Profile</a></Link></li>
                                            <li><Link href="/settings"><a><i className="fas fa-cog"></i> Settings</a></Link></li>
                                            <li><a onClick={logout}><i className="fas fa-sign-out-alt"></i> Log Out</a></li>
                                        </ul>
                                    </div>
                                    <button className="toggle-bar">
                                        <span className="line">Test 1</span>
                                        <span className="line">Test 2</span>
                                        <span className="line">Test 3</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        </>
    )
}
