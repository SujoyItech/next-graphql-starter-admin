import Link from "next/link";
import defaultAvatar from "../../public/assets/images/dashbord/profile-image.png"
import Image from "next/image";

export default function ProfileSection({user}:any){
    return (
        <>
            <main className="dashboard-main-content">
                <div className="container-fluid">
                    <div className="dashboard-sectoin-wrap">
                        <div className="profile-page-area">
                            <div className="row">
                                <div className="col-md-4 col-lg-3">
                                    <div className="profile-page-left">
                                        <h2 className="dashbord-section-title">My Profile</h2>
                                        <div className="profile-image">
                                            <Image src={user && user.avatar ? user.avatar : defaultAvatar} alt="avater" layout="fill"/>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-8 col-lg-9">
                                    <div className="profile-page-right">
                                        <h2 className="dashbord-section-title">General Information</h2>
                                        <div className="section-inner-wrap">
                                            <h3 className="section-inner-title">Profile Information</h3>
                                            <div className="profile-info">
                                                <div className="table-responsive">
                                                    <table className="table">
                                                        <tbody>
                                                        <tr>
                                                            <td>Name</td>
                                                            <td>:</td>
                                                            <td><strong>{user && user.name}</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Email</td>
                                                            <td>:</td>
                                                            <td>{user && user.email}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Phone</td>
                                                            <td>:</td>
                                                            <td>{user && user.phone}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Role</td>
                                                            <td>:</td>
                                                            <td>User</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Status</td>
                                                            <td>:</td>
                                                            <td><span>Active</span></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="sectoin-bottom text-center">
                                            <Link href="/profile/edit">
                                                <a className="primary-btn">Edit Profile</a>
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </>
    )
}