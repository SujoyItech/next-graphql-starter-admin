export default function TransactionSection(){
    return(
        <>
            <main className="dashboard-main-content">
                <div className="container-fluid">
                    <div className="transaction-history">
                        <div className="secondary-table">
                            <div className="table-top-bar">
                                <div className="table-top-wrap">
                                    <div className="table-top-left">
                                        <h4 className="table-section-title">Transaction History</h4>
                                    </div>
                                    <div className="search-field">
                                        <input type="search" className="form-control" id="search" placeholder="Search"/>
                                        <button className="search-btn"><i className="fas fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div className="table-responsive">
                                <table className="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Image</th>
                                        <th scope="col">Address</th>
                                        <th scope="col">From</th>
                                        <th scope="col">To</th>
                                        <th scope="col">Value</th>
                                        <th scope="col">Fee</th>
                                        <th scope="col">Time</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><img className="avater-image" src="/assets/images/avater.png" alt="avater"/>
                                        </td>
                                        <td><span>0x15c2adbe81d26a07d4...</span></td>
                                        <td><span>@Felis congue</span></td>
                                        <td><span>@ayoub fennouni</span></td>
                                        <td><span>2.5 Ether</span></td>
                                        <td><span>0.00216855</span></td>
                                        <td><span>12:34:08 PM</span></td>
                                    </tr>
                                    <tr>
                                        <td><img className="avater-image" src="/assets/images/avater.png" alt="avater"/>
                                        </td>
                                        <td><span>0x15c2adbe81d26a07d4...</span></td>
                                        <td><span>@Felis congue</span></td>
                                        <td><span>@ayoub fennouni</span></td>
                                        <td><span>2.5 Ether</span></td>
                                        <td><span>0.00216855</span></td>
                                        <td><span>12:34:08 PM</span></td>
                                    </tr>
                                    <tr>
                                        <td><img className="avater-image" src="/assets/images/avater.png" alt="avater"/>
                                        </td>
                                        <td><span>0x15c2adbe81d26a07d4...</span></td>
                                        <td><span>@Felis congue</span></td>
                                        <td><span>@ayoub fennouni</span></td>
                                        <td><span>2.5 Ether</span></td>
                                        <td><span>0.00216855</span></td>
                                        <td><span>12:34:08 PM</span></td>
                                    </tr>
                                    <tr>
                                        <td><img className="avater-image" src="/assets/images/avater.png" alt="avater"/>
                                        </td>
                                        <td><span>0x15c2adbe81d26a07d4...</span></td>
                                        <td><span>@Felis congue</span></td>
                                        <td><span>@ayoub fennouni</span></td>
                                        <td><span>2.5 Ether</span></td>
                                        <td><span>0.00216855</span></td>
                                        <td><span>12:34:08 PM</span></td>
                                    </tr>
                                    <tr>
                                        <td><img className="avater-image" src="/assets/images/avater.png" alt="avater"/>
                                        </td>
                                        <td><span>0x15c2adbe81d26a07d4...</span></td>
                                        <td><span>@Felis congue</span></td>
                                        <td><span>@ayoub fennouni</span></td>
                                        <td><span>2.5 Ether</span></td>
                                        <td><span>0.00216855</span></td>
                                        <td><span>12:34:08 PM</span></td>
                                    </tr>
                                    <tr>
                                        <td><img className="avater-image" src="/assets/images/avater.png" alt="avater"/>
                                        </td>
                                        <td><span>0x15c2adbe81d26a07d4...</span></td>
                                        <td><span>@Felis congue</span></td>
                                        <td><span>@ayoub fennouni</span></td>
                                        <td><span>2.5 Ether</span></td>
                                        <td><span>0.00216855</span></td>
                                        <td><span>12:34:08 PM</span></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </>
    )
}