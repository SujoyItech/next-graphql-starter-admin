import {Col, Row} from "react-bootstrap";
import HyperDatepicker from "../../src/components/Datepicker";
import React from "react";

export default function DashboardSection(){
    return(
        <>
            <Row>
                <Col>
                    <div className="page-title-box justify-content-center d-flex text-center">
                        <h4 className="page-title">Dashboard</h4>
                    </div>
                </Col>
            </Row>
        </>
    )
}
