import UpdatePasswordForm from "../../components/Profile/UpdatePasswordForm";

export default function ChangePasswordSection(){
    return (
        <>
            <main className="dashboard-main-content">
                <div className="container-fluid">
                    <div className="dashboard-sectoin-wrap">

                        <div className="dashbord-form">
                            <div className="row">
                                <div className="col-lg-8 offset-lg-2">
                                    <h2 className="dashbord-section-title">Reset Password</h2>
                                    <UpdatePasswordForm/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </>
    )
}