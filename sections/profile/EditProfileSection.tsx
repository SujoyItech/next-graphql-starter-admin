import EditProfileForm from "../../components/Profile/EditProfileForm";
import {useEffect, useState} from "react";
import {useStaffQuery} from "../../src/graphql/generated";
import {Spinner} from "react-bootstrap";
import Loader from "../../components/Loader";

export default function EditProfileSection(){
    const [me,setMe] = useState<any>(undefined);
    const {data,isSuccess} = useStaffQuery()
    useEffect(()=>{
        if (isSuccess){
            setMe(data?data.staff : undefined);
        }
    },[data])
    return (
        <>
            <main className="dashboard-main-content">
                <div className="container-fluid">
                    <div className="dashboard-sectoin-wrap">
                        <div className="profile-page-area">
                            {
                                me ? <EditProfileForm user = {me} /> : <Loader/>
                            }
                        </div>
                    </div>
                </div>
            </main>
        </>
    )
}