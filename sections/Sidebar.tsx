import Link from "next/link";
import MetisMenu from "@metismenu/react";
import 'metismenujs/dist/metismenujs.css';
export default function Sidebar(){
    return (
        <>
            <aside className="dashboard-sidebar-area">
                <nav className="menu-area">
                    <MetisMenu>
                        <li>
                            <Link href="/dashboard">
                                <a><img className="menu-icon" src="/assets//images/dashbord/dashboard.svg" alt="icon"/><span>Dashboard</span></a>
                            </Link>
                        </li>

                        <li className="active">
                            <Link href="/profile">
                                <a><img className="menu-icon" src="/assets//images/dashbord/user.svg" alt="icon"/><span>My Profile</span></a>
                            </Link>
                        </li>

                        <li>
                            <Link href="/profile/change-password">
                                <a><img className="menu-icon" src="/assets//images/dashbord/settings.svg" alt="icon"/><span>Change Password</span></a>
                            </Link>
                        </li>

                        <li>
                            <Link href="/role">
                                <a><img className="menu-icon" src="/assets//images/dashbord/settings.svg" alt="icon"/><span>Role management</span></a>
                            </Link>
                        </li>

                        <li>
                            <Link href="#">
                                <a className="has-arrow"><img className="menu-icon" src="/assets//images/dashbord/settings.svg" alt="icon"/><span>Settings</span></a>
                            </Link>
                            <ul>
                                <li><Link href="/settings/application"><a><img className="menu-icon" src="/assets//images/dashbord/settings.svg" alt="icon"/><span>Application Settings</span></a></Link></li>
                                <li><Link href="/settings/email"><a><img className="menu-icon" src="/assets//images/dashbord/settings.svg" alt="icon"/><span>Logo Settings</span></a></Link></li>
                                <li><Link href="/settings/logo"><a><img className="menu-icon" src="/assets//images/dashbord/settings.svg" alt="icon"/><span>Email Settings</span></a></Link></li>
                            </ul>
                        </li>
                    </MetisMenu>
                </nav>
            </aside>
        </>
    )
}
