import MediaCard from "../components/Media/MediaCard";
import React, {useEffect, useState} from "react";
import {useListFileQuery} from "../src/graphql/generated";

export default function MediaListSection(){
    const listFileQuery = useListFileQuery();
    const[loading, setLoading] = useState(true);
    useEffect(()=>{
        if (listFileQuery.status === "success"){
            setLoading(false);
        }
    },[listFileQuery])
    return(
        <>
            <div className="row">
                {
                    !loading &&
                    listFileQuery.data?.listFile?
                        listFileQuery.data.listFile.map((media)=>{
                            return (
                                <MediaCard key={media.name} url={media.url}/>
                            )
                        }) : 'Loading...'
                }

                <div className="col-lg-12 text-center mt-20">
                    <div className="pagination-area">
                        <ul className="pagination-page">
                            <li><a href="#"><i className="fas fa-angle-left"></i></a></li>
                            <li className="active"><a href="#">1</a></li>
                            <li><span className="space"></span></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">6</a></li>
                            <li><a href="#">7</a></li>
                            <li><a href="#"><i className="fas fa-angle-right"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </>
    )
}