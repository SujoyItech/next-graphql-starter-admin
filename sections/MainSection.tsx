import React, {Suspense, useEffect} from "react";
import {Container} from "react-bootstrap";
import {ShowToastAlert} from "../src/Services/ToastAlertService";
const Topbar = React.lazy(() => import('../layouts/Topbar'));
const LeftSidebar = React.lazy(() => import('../layouts/LeftSidebar'));
const Footer = React.lazy(() => import('../layouts/Footer'));

export default function MainSection({children} : any){
    useEffect(() => {
        if (document.body) document.body.classList.remove('authentication-bg', 'authentication-bg-pattern');
    }, []);

    ShowToastAlert();
    const loading = () => <div className="text-center"></div>;
    return(
        <div id="wrapper">
            <Suspense fallback={loading()}>
                <Topbar
                    navCssClasses="topnav-navbar topnav-navbar-dark"
                    topbarDark={true}
                    hideLogo={false}
                />
            </Suspense>
            <Suspense fallback={loading()}>
                <LeftSidebar/>
            </Suspense>

            <div className="content-page">
                <div className="content">
                    <Container fluid>
                        <Suspense fallback={loading()}>{children}</Suspense>
                    </Container>
                </div>

                <Suspense fallback={loading()}>
                    <Footer />
                </Suspense>
            </div>
        </div>
    )
}
