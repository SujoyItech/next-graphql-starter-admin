import {useToasts} from "react-toast-notifications";
import React, {useState} from "react";
import {useDeleteRoleMutation, useGetRolesQuery} from "../../src/graphql/generated";
import Link from "next/link";
import PageTitle from "../../src/components/PageTitle";
import {Card, Col, Row, Table} from "react-bootstrap";

export default function RoleSection(){
    const {addToast} = useToasts();
    const[confirmAlert, setConfirmAlert] = useState(false);
    const[roles, setRoles] = useState<any>(undefined);
    const [query, setQuery] = useState('')
    const [sort, setSort] = useState({
        field: 'id',
        direction: 'desc'
    });
    const filterData = {query, orderBy: sort}
    useGetRolesQuery({},{
        onSuccess({getRoles}){
            setRoles(getRoles);
        }
    });

    const deleteRoleMutation = useDeleteRoleMutation();

    const deleteRole = async (roleId: number)=>{
        try{
            const response = await deleteRoleMutation.mutateAsync({
                id : roleId
            });
            if (response.deleteRole.success === true){
                addToast(response.deleteRole.message,{appearance: 'success'});
            }else{
                addToast(response.deleteRole.message,{appearance: 'error'});
            }
        }catch (error:any){
            addToast(error.message,{appearance: 'error'});
        }
    }

    return(
        <>
            <React.Fragment>
                <PageTitle
                    breadCrumbItems={[
                        { label: 'Roles & Permissions', path: '/roles',active: true },
                    ]}
                    title={'Roles & Permission'}
                />

                <Row>
                    <Col lg={12}>
                        <Card>
                            <Card.Body>
                                <Row>
                                    <Col md={6} lg={6}>
                                        <h4 className="header-title">Roles & permission</h4>
                                        <p className="text-muted font-14 mb-4">
                                            Here goes the list of roles & permissions..
                                        </p>
                                    </Col>
                                    <Col md={6} lg={6}>
                                        <div className="d-flex align-items-center justify-content-end">
                                            <div className="active-pink-3 active-pink-4 me-2">
                                                <input className="form-control" type="text" placeholder="Search" aria-label="Search" />
                                            </div>
                                            <Link href='role/create'><button className="btn btn-dark"><i className="fas fa-plus"></i> Create Role</button></Link>
                                        </div>
                                    </Col>
                                </Row>
                                <div className="table-responsive">
                                    <Table className="mb-0">
                                        <thead className="table-dark">
                                        <tr>
                                            <th>Name</th>
                                            <th>Permission</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            roles && roles.length > 0 ? roles.map((role : any)=>{
                                                return(
                                                    <tr key={role.id}>
                                                        <td>{role.name}</td>
                                                        <td>{role.permissions}</td>
                                                        <td>
                                                            <Link href={`/role/edit/${role.id}`}><button className="btn btn-xs btn-info mx-1"><i className="fas fa-edit"></i></button></Link>
                                                            <button className="btn btn-danger btn-xs mx-1" onClick={()=>deleteRole(role.id)} ><i className="fas fa-trash"></i> </button>
                                                        </td>
                                                    </tr>
                                                )
                                            }) : null
                                        }
                                        </tbody>
                                    </Table>
                                </div>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>

            </React.Fragment>
        </>
    )
}
