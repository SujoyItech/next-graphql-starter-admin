export default function FooterSection(){
    return(
        <>
            <footer className="dashboard-footer">
                <p className="copyright-text">&copy; 2021. All Rights Reserved by <a href="#">Itech</a></p>
            </footer>
        </>
    )
}