import {useToasts} from "react-toast-notifications";
import React, {useState} from "react";
import {
    useDeleteStaffMutation,
    useGetStaffListsQuery
} from "../../src/graphql/generated";
import Link from "next/link";
import PageTitle from "../../src/components/PageTitle";
import {Card, Col, Row, Table} from "react-bootstrap";

export default function StaffSection(){
    const {addToast} = useToasts();
    const[confirmAlert, setConfirmAlert] = useState(false);
    const[staffs, setStaff] = useState<any>(undefined);
    const [query, setQuery] = useState('')
    const [sort, setSort] = useState({
        field: 'id',
        direction: 'desc'
    });
    const filterData = {query, orderBy: sort}
    useGetStaffListsQuery({},{
        onSuccess({getStaffLists}){
            setStaff(getStaffLists);
        }
    });

    const deleteStaffMutation = useDeleteStaffMutation();

    const deleteStaff = async (staffId: number)=>{
        try{
            const response = await deleteStaffMutation.mutateAsync({
                id : staffId
            });
            if (response.deleteStaff.success === true){
                addToast(response.deleteStaff.message,{appearance: 'success'});
            }else{
                addToast(response.deleteStaff.message,{appearance: 'error'});
            }
        }catch (error:any){
            addToast(error && error.message,{appearance: 'error'});
        }
    }

    return(
        <>
            <React.Fragment>
                <PageTitle
                    breadCrumbItems={[
                        { label: 'Staff management', path: '/staffs',active: true },
                    ]}
                    title={'Staff Management'}
                />

                <Row>
                    <Col lg={12}>
                        <Card>
                            <Card.Body>
                                <Row>
                                    <Col md={6} lg={6}>
                                        <h4 className="header-title">Staff</h4>
                                        <p className="text-muted font-14 mb-4">
                                            Here goes the list of staffs.
                                        </p>
                                    </Col>
                                    <Col md={6} lg={6}>
                                        <div className="d-flex align-items-center justify-content-end">
                                            <div className="active-pink-3 active-pink-4 me-2">
                                                <input className="form-control" type="text" placeholder="Search" aria-label="Search" />
                                            </div>
                                            <Link href='role/create'><button className="btn btn-dark"><i className="fas fa-plus"></i> Create Staff</button></Link>
                                        </div>
                                    </Col>
                                </Row>
                                <div className="table-responsive">
                                    <Table className="mb-0">
                                        <thead className="table-dark">
                                        <tr>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Role</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            staffs && staffs.length > 0 ? staffs.map((staff : any)=>{
                                                return(
                                                    <tr key={staff.id}>
                                                        <td><img src={staff.avatar ? staff.avatar : '/assets/images/author.png'} className="avatar avatar-sm"/></td>
                                                        <td>{staff.name}</td>
                                                        <td>{staff.username}</td>
                                                        <td>{staff.email}</td>
                                                        <td>{staff.phone}</td>
                                                        <td>{staff.role?.name}</td>
                                                        <td>
                                                            <Link href={`/staff/edit/${staff.id}`}><button className="btn btn-xs btn-info mx-1"><i className="fas fa-edit"></i></button></Link>
                                                            <button className="btn btn-danger btn-xs mx-1" onClick={()=>deleteStaff(staff.id)} ><i className="fas fa-trash"></i> </button>
                                                        </td>
                                                    </tr>
                                                )
                                            }) : null
                                        }
                                        </tbody>
                                    </Table>
                                </div>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>

            </React.Fragment>
        </>
    )
}
