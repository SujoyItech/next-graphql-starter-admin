export interface MenuItemTypes {
    key: string;
    label: string;
    isTitle?: boolean;
    icon?: string;
    url?: string;
    badge?: {
        variant: string;
        text: string;
    };
    parentKey?: string;
    target?: string;
    children?: MenuItemTypes[];
}

const MENU_ITEMS: MenuItemTypes[] = [
    { key: 'navigation', label: 'Navigation', isTitle: true },
    {
        key: 'dashboard',
        label: 'Dashboard',
        isTitle: false,
        icon: 'airplay',
        url: '/dashboard',
    },
    { key: 'apps', label: 'Apps', isTitle: true },
    {
        key: 'settings',
        label: 'Settings',
        isTitle: false,
        icon: 'settings',
        children: [
            {
                key: 'application-settings',
                label: 'Application Settings',
                url: '/settings/application',
                parentKey: 'settings',
            },
            {
                key: 'email-settings',
                label: 'Email Settings',
                url: '/settings/email',
                parentKey: 'settings',
            },
            {
                key: 'logo-settings',
                label: 'Logo Settings',
                url: '/settings/logo',
                parentKey: 'settings',
            },
        ],
    },
    {
        key: 'roles',
        label: 'Role Management',
        isTitle: false,
        icon: 'users',
        url: '/role',
    },
    {
        key: 'staffs',
        label: 'Staff Management',
        isTitle: false,
        icon: 'users',
        url: '/staff',
    },
];

export { MENU_ITEMS};
