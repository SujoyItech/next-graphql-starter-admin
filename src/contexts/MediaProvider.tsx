import PropTypes from 'prop-types';
import {createContext, ReactNode, useContext, useState} from 'react';

type collectionContextType = {
    getMediaImage: ()=> string,
    setMediaImage: (url : any)=>void,
};

const collectionContextDefaultValues: collectionContextType = {
    getMediaImage: ()=>{return ''},
    setMediaImage: ()=>{}
};

type Props = {
    children: ReactNode;
};

const MediaContext = createContext<collectionContextType>(collectionContextDefaultValues);

const MediaProvider = ({ children } : Props) => {
    const[imageUrl, setImageUrl] = useState<string>('');

    const getMediaImage = ()=>{
        return imageUrl;
    }

    const setMediaImage = (url:string) => {
        setImageUrl(url)
    }

    return (
        // @ts-ignore
        <MediaContext.Provider value={{ getMediaImage,setMediaImage}}>
            {children}
        </MediaContext.Provider>
    );
};

MediaProvider.propTypes = {
    children: PropTypes.object,
};

const useMedia = () => useContext(MediaContext);

export { MediaProvider as default, useMedia };
