import PropTypes from 'prop-types';
import {createContext, ReactNode, useContext, useEffect, useState} from 'react';

type appContextType = {
    openMenu: boolean,
    setOpenMenu: ()=>void,
    isCondensed: boolean,
    setCondensed: ()=>void,
};

const appContextDefaultValues: appContextType = {
    openMenu: false,
    setOpenMenu: ()=>{},
    isCondensed: false,
    setCondensed: ()=>{},
};

type Props = {
    children: ReactNode;
};

const AppContext = createContext<any>(appContextDefaultValues);

const AppProvider = ({ children } : Props) => {
    const [openMenu,setOpenMenu] = useState(false);
    const [isCondensed,setCondensed] = useState(false);
    const changeBodyAttribute = (attribute: string, value: string): void => {
        if (document.body) document.body.setAttribute(attribute, value);
    };

    useEffect(() => {
        if (isCondensed){
            changeBodyAttribute('data-sidebar-size','condensed')
        }else{
            changeBodyAttribute('data-sidebar-size','default')
        }
    }, [isCondensed]);

    return (
        // @ts-ignore
        <AppContext.Provider value={{ openMenu,setOpenMenu,isCondensed,setCondensed}}>
            {children}
        </AppContext.Provider>
    );
};

AppProvider.propTypes = {
    children: PropTypes.object,
};

const useApp = () => useContext(AppContext);

export { AppProvider as default, useApp };
