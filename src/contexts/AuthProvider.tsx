import PropTypes, {any} from 'prop-types';
import {createContext, ReactNode, useContext, useEffect, useState} from 'react';
import {SignInInput} from '../Interfaces/AuthInterface';
import {useStaffLoginMutation} from "../graphql/generated";

interface responseInterface{
    success : boolean,
    message : string | null,
    error?: any,
    data?: any
}
type authContextType = {
    authState: boolean;
    isAuthenticated : () => any,
    signIn: (payload : SignInInput) => any;
    signOut: () => any;
};

const authContextDefaultValues: authContextType = {
    authState: false,
    isAuthenticated: () => {return false},
    signIn: () => {return null},
    signOut: () => {return null},
};

type Props = {
    children: ReactNode;
};

const AuthContext = createContext<authContextType>(authContextDefaultValues);

const AuthProvider = ({ children } : Props) => {
    const [authState , setAuthState] = useState(false);
    const loginMutation = useStaffLoginMutation();

    useEffect(() => {
        if (isAuthenticated()) {
            setAuthState(true);
        }

    }, []);

    const signIn = async (payload:any) => {
        try {
            const response = await loginMutation.mutateAsync(payload);
            const accessToken = response.StaffLogin.accessToken
            const refreshToken = response.StaffLogin.refreshToken;
            const expiredAt = response.StaffLogin.expireAt;
            const token = {
                accessToken : accessToken,
                refreshToken: refreshToken,
                expiredAt: expiredAt
            }
            addAuthToLocalStorage(token);
            setAuthState(true);
            return {
                success : true,
                message : 'Login successful!'
            }
        }catch (error:any){
            return {
                success : false,
                message : error.message
            }
        }
    };

    const isAuthenticated = ()=>{
        const token = localStorage.getItem('token');
        const tokenObject = token ? JSON.parse(token) : null;
        if (tokenObject){
            // @ts-ignore
            if (new Date(tokenObject.expiredAt) >= new Date(Date.now())) {
                return true;
            }else {
                 removeAuthFromLocalStorage();
            }
              removeAuthFromLocalStorage();
        }
        return false;
    }

    const signOut = async () => {
        setAuthState(false);
        removeAuthFromLocalStorage();
    };

    const addAuthToLocalStorage =  async (token: object)=>{
        localStorage.setItem('token', JSON.stringify(token));
    }
    const removeAuthFromLocalStorage = ()=>{
        localStorage.removeItem('token');
    }

    return (
        <AuthContext.Provider value={{ authState, isAuthenticated, signIn, signOut }}>
            {children}
        </AuthContext.Provider>
    );
};

AuthProvider.propTypes = {
    children: PropTypes.object,
};

const useAuth = () => useContext(AuthContext);

export { AuthProvider as default, useAuth };
