import {useSignupMutation} from "../graphql/generated";
import {useState} from "react";

interface SignupDataResponse{
    success : boolean,
    message : string,
    data : object | null
}

export async function SignupService (payload : any){
    const [signupMutation,setSignUpMutation] = useState(useSignupMutation());

    const data = {
        name : payload.name,
        username : payload.name,
        email : payload.email,
        password: payload.password
    }
    console.log(payload);
    // eslint-disable-next-line react-hooks/rules-of-hooks
    try {
        // @ts-ignore
        const mutation = signupMutation();
        console.log(mutation);
        const response = await mutation.mutateAsync(data);
        console.log(response);
    }catch (error){
        console.log(error);
    }
    console.log('faltu');
}
