import { useMutation, UseMutationOptions, useQuery, UseQueryOptions, useInfiniteQuery, UseInfiniteQueryOptions, QueryFunctionContext } from 'react-query';
import { graphqlFetcher } from '../lib/fetcher';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** Date custom scalar type */
  Date: any;
  /** The `Upload` scalar type represents a file upload. */
  Upload: any;
};

export type Bid = {
  __typename?: 'Bid';
  amount: Scalars['Float'];
  /** Identifies the date and time when the object was created. */
  createdAt: Scalars['Date'];
  id: Scalars['Int'];
  item?: Maybe<Item>;
  itemId: Scalars['Int'];
  sender?: Maybe<User>;
  senderId: Scalars['Int'];
  status: Scalars['String'];
  /** Identifies the date and time when the object was last updated. */
  updatedAt: Scalars['Date'];
};

export type Category = {
  __typename?: 'Category';
  /** Identifies the date and time when the object was created. */
  createdAt: Scalars['Date'];
  id: Scalars['Int'];
  image: Scalars['String'];
  items?: Maybe<Array<Item>>;
  title: Scalars['String'];
  /** Identifies the date and time when the object was last updated. */
  updatedAt: Scalars['Date'];
};

export type CategoryCreateInput = {
  image?: InputMaybe<Scalars['String']>;
  title: Scalars['String'];
};

export type Collection = {
  __typename?: 'Collection';
  author?: Maybe<User>;
  authorId: Scalars['Int'];
  contractAddress?: Maybe<Scalars['String']>;
  contractSymbol?: Maybe<Scalars['String']>;
  coverImage?: Maybe<Scalars['String']>;
  /** Identifies the date and time when the object was created. */
  createdAt: Scalars['Date'];
  description?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  image?: Maybe<Scalars['String']>;
  items?: Maybe<Array<Item>>;
  name: Scalars['String'];
  note?: Maybe<Scalars['String']>;
  royalties: Scalars['Float'];
  status: Scalars['String'];
  /** Identifies the date and time when the object was last updated. */
  updatedAt: Scalars['Date'];
  volumeTraded: Scalars['Int'];
};

export type CollectionConnection = {
  __typename?: 'CollectionConnection';
  edges?: Maybe<Array<CollectionEdge>>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type CollectionEdge = {
  __typename?: 'CollectionEdge';
  cursor: Scalars['String'];
  node: Collection;
};

export type CollectionOrder = {
  direction: OrderDirection;
  field: CollectionOrderField;
};

/** Properties by which collection connections can be ordered. */
export enum CollectionOrderField {
  CreatedAt = 'createdAt',
  Id = 'id',
  Loyalties = 'loyalties',
  Name = 'name'
}

export type CollectionWithMeta = {
  __typename?: 'CollectionWithMeta';
  collection: Collection;
  itemCount: Scalars['Int'];
};

export type CollectorWithItem = {
  __typename?: 'CollectorWithItem';
  collector: User;
  items: ItemConnection;
};

export type CreateDto = {
  auctionEndAt?: InputMaybe<Scalars['Date']>;
  categoryId: Scalars['Int'];
  collectionId: Scalars['Int'];
  description?: InputMaybe<Scalars['String']>;
  image: Scalars['String'];
  levels?: InputMaybe<Array<RangeDto>>;
  mediaPath: Scalars['String'];
  name: Scalars['String'];
  price: Scalars['Float'];
  properties?: InputMaybe<Array<PropertyDto>>;
  stats?: InputMaybe<Array<RangeDto>>;
};

export type FileObject = {
  __typename?: 'FileObject';
  name: Scalars['String'];
  type: Scalars['String'];
  url: Scalars['String'];
  variants?: Maybe<Array<FileVariant>>;
};

export type FileVariant = {
  __typename?: 'FileVariant';
  type: Scalars['String'];
  url: Scalars['String'];
};

export type History = {
  __typename?: 'History';
  amount: Scalars['String'];
  /** Identifies the date and time when the object was created. */
  createdAt: Scalars['Date'];
  from?: Maybe<User>;
  fromId: Scalars['Int'];
  hash?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  isValid: Scalars['Boolean'];
  item?: Maybe<Item>;
  itemId: Scalars['Int'];
  to?: Maybe<User>;
  toId?: Maybe<Scalars['Int']>;
  type: Scalars['String'];
  /** Identifies the date and time when the object was last updated. */
  updatedAt: Scalars['Date'];
};

export type Item = {
  __typename?: 'Item';
  auctionEndAt?: Maybe<Scalars['Date']>;
  bids?: Maybe<Array<Bid>>;
  category?: Maybe<Category>;
  categoryId: Scalars['Int'];
  collection?: Maybe<Collection>;
  collectionId: Scalars['Int'];
  createdAt: Scalars['Date'];
  creator?: Maybe<User>;
  creatorId: Scalars['Int'];
  currentOwner?: Maybe<User>;
  currentOwnerId: Scalars['Int'];
  description?: Maybe<Scalars['String']>;
  externalUrl?: Maybe<Scalars['String']>;
  histories?: Maybe<Array<History>>;
  id: Scalars['Int'];
  image?: Maybe<Scalars['String']>;
  ipfsHash?: Maybe<Scalars['String']>;
  levels?: Maybe<Array<ItemLevel>>;
  likeCount: Scalars['Int'];
  likes?: Maybe<Array<Like>>;
  mediaPath?: Maybe<Scalars['String']>;
  mintedAt?: Maybe<Scalars['Date']>;
  name: Scalars['String'];
  price: Scalars['Float'];
  prices?: Maybe<Array<Price>>;
  properties?: Maybe<Array<ItemProperty>>;
  stats?: Maybe<Array<ItemStat>>;
  status: Scalars['String'];
  tokenId?: Maybe<Scalars['Int']>;
  unlockContentUrl?: Maybe<Scalars['String']>;
  updatedAt?: Maybe<Scalars['Date']>;
  viewCount: Scalars['Int'];
};

export type ItemConnection = {
  __typename?: 'ItemConnection';
  edges?: Maybe<Array<ItemEdge>>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type ItemEdge = {
  __typename?: 'ItemEdge';
  cursor: Scalars['String'];
  node: Item;
};

export type ItemLevel = {
  __typename?: 'ItemLevel';
  id: Scalars['ID'];
  item?: Maybe<Item>;
  itemId: Scalars['Int'];
  name: Scalars['String'];
  value: Scalars['Float'];
  valueof: Scalars['Float'];
};

export type ItemOrder = {
  direction: OrderDirection;
  field: ItemOrderField;
};

/** Properties by which Item connections can be ordered. */
export enum ItemOrderField {
  Id = 'id',
  MintedAt = 'mintedAt',
  Name = 'name',
  Price = 'price',
  TokenId = 'tokenId',
  View = 'view'
}

export type ItemProperty = {
  __typename?: 'ItemProperty';
  id: Scalars['ID'];
  item?: Maybe<Item>;
  itemId: Scalars['Int'];
  name: Scalars['String'];
  type: Scalars['String'];
};

export type ItemStat = {
  __typename?: 'ItemStat';
  id: Scalars['ID'];
  item?: Maybe<Item>;
  itemId: Scalars['Int'];
  name: Scalars['String'];
  value: Scalars['Float'];
  valueof: Scalars['Float'];
};

export type Like = {
  __typename?: 'Like';
  id: Scalars['ID'];
  item?: Maybe<Item>;
  itemId: Scalars['Int'];
  user?: Maybe<User>;
  userId: Scalars['Int'];
};

export type LoginInput = {
  password: Scalars['String'];
  username: Scalars['String'];
};

export type Mutation = {
  __typename?: 'Mutation';
  StaffLogin: Token;
  changePassword: ResponseMessageWithStatusModel;
  createCategory: Category;
  createCollection: Collection;
  createRole: ResponseMessageWithStatusModel;
  createStaff: Staff;
  createWallet: Scalars['String'];
  deleteCategory: Category;
  deleteRole: ResponseMessageWithStatusModel;
  deleteStaff: ResponseMessageWithStatusModel;
  deployCollection: Scalars['String'];
  enableAuction: Scalars['Boolean'];
  login: Token;
  makeWin: Scalars['Boolean'];
  placeBid: Bid;
  publishItem: Scalars['String'];
  refreshToken: Token;
  removeMyBid: Scalars['Boolean'];
  sendForgetPasswordMail: ResponseMessageWithStatusModel;
  sendVerificationMail: ResponseMessageWithStatusModel;
  signup: Token;
  storeItem: Item;
  toggleLike: Scalars['Boolean'];
  updateCategory: Category;
  updatePassword: ResponseMessageWithStatusModel;
  updateProfile: ResponseMessageWithStatusModel;
  updateRole: ResponseMessageWithStatusModel;
  updateStaff: ResponseMessageWithStatusModel;
  updateStaffPassword: ResponseMessageWithStatusModel;
  updateStaffProfile: ResponseMessageWithStatusModel;
  uploadFile: FileObject;
  verifyUserEmail: ResponseMessageWithStatusModel;
};


export type MutationStaffLoginArgs = {
  data: StaffLoginInput;
};


export type MutationChangePasswordArgs = {
  data: ResetPasswordInput;
};


export type MutationCreateCategoryArgs = {
  data: CategoryCreateInput;
};


export type MutationCreateCollectionArgs = {
  data: CollectionInput;
};


export type MutationCreateRoleArgs = {
  data: RoleInput;
};


export type MutationCreateStaffArgs = {
  data: StaffCreateInput;
};


export type MutationDeleteCategoryArgs = {
  id: Scalars['Int'];
};


export type MutationDeleteRoleArgs = {
  id: Scalars['Int'];
};


export type MutationDeleteStaffArgs = {
  id: Scalars['Int'];
};


export type MutationDeployCollectionArgs = {
  collectionId: Scalars['Int'];
};


export type MutationEnableAuctionArgs = {
  date: Scalars['Date'];
  itemId: Scalars['Int'];
};


export type MutationLoginArgs = {
  data: LoginInput;
};


export type MutationPlaceBidArgs = {
  amount: Scalars['Float'];
  itemId: Scalars['Int'];
};


export type MutationPublishItemArgs = {
  itemId: Scalars['Int'];
};


export type MutationRefreshTokenArgs = {
  token: Scalars['String'];
};


export type MutationRemoveMyBidArgs = {
  itemId: Scalars['Int'];
};


export type MutationSendForgetPasswordMailArgs = {
  email: Scalars['String'];
};


export type MutationSignupArgs = {
  data: SignupInput;
};


export type MutationStoreItemArgs = {
  data: CreateDto;
};


export type MutationToggleLikeArgs = {
  itemId: Scalars['Int'];
  liked: Scalars['Boolean'];
};


export type MutationUpdateCategoryArgs = {
  data: CategoryCreateInput;
  id: Scalars['Int'];
};


export type MutationUpdatePasswordArgs = {
  data: UpdatePasswordInput;
};


export type MutationUpdateProfileArgs = {
  data: UpdateProfileInput;
};


export type MutationUpdateRoleArgs = {
  data: RoleInput;
  id: Scalars['Int'];
};


export type MutationUpdateStaffArgs = {
  data: StaffUpdateInput;
  id: Scalars['Int'];
};


export type MutationUpdateStaffPasswordArgs = {
  data: UpdatePasswordInput;
};


export type MutationUpdateStaffProfileArgs = {
  data: StaffUpdateInput;
};


export type MutationUploadFileArgs = {
  file: Scalars['Upload'];
};


export type MutationVerifyUserEmailArgs = {
  code: Scalars['Int'];
};

/** Possible directions in which to order a list of items when provided an `orderBy` argument. */
export enum OrderDirection {
  Asc = 'asc',
  Desc = 'desc'
}

export type PageInfo = {
  __typename?: 'PageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

export type Price = {
  __typename?: 'Price';
  amount: Scalars['Float'];
  /** Identifies the date and time when the object was created. */
  createdAt: Scalars['Date'];
  id: Scalars['Int'];
  item?: Maybe<Scalars['Float']>;
  itemId: Scalars['Int'];
  /** Identifies the date and time when the object was last updated. */
  updatedAt: Scalars['Date'];
};

export type PropertyDto = {
  name: Scalars['String'];
  type: Scalars['String'];
};

export type Query = {
  __typename?: 'Query';
  getCategories: Array<Category>;
  getCollection: CollectionWithMeta;
  getCollectionLists: CollectionConnection;
  getCollectorList: UserConnection;
  getCollectorWithItem: CollectorWithItem;
  getRole: Role;
  getRoles: Array<Role>;
  getSettings: SettingsDto;
  getStaff: Staff;
  getStaffLists: Array<Staff>;
  listFile: Array<FileObject>;
  listItems: ItemConnection;
  me: User;
  myCollectionLists: Array<Collection>;
  myCollections: CollectionConnection;
  ownedItems: ItemConnection;
  singleItem: Item;
  staff: Staff;
  walletBalance: Scalars['Float'];
};


export type QueryGetCollectionArgs = {
  address: Scalars['String'];
};


export type QueryGetCollectionListsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CollectionOrder>;
  paginateNumber?: InputMaybe<Scalars['Int']>;
  query?: InputMaybe<Scalars['String']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type QueryGetCollectorListArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  paginateNumber?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type QueryGetCollectorWithItemArgs = {
  address: Scalars['String'];
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ItemOrder>;
  paginateNumber?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type QueryGetRoleArgs = {
  id: Scalars['Int'];
};


export type QueryGetRolesArgs = {
  orderBy?: InputMaybe<RoleOrder>;
  query?: InputMaybe<Scalars['String']>;
};


export type QueryGetStaffArgs = {
  id: Scalars['Int'];
};


export type QueryListItemsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  categoryId?: InputMaybe<Scalars['Int']>;
  collectionId?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  isLiveAuction?: InputMaybe<Scalars['Boolean']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ItemOrder>;
  query?: InputMaybe<Scalars['String']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type QueryMyCollectionListsArgs = {
  orderBy?: InputMaybe<CollectionOrder>;
};


export type QueryMyCollectionsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CollectionOrder>;
  query?: InputMaybe<Scalars['String']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type QueryOwnedItemsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  categoryId?: InputMaybe<Scalars['Int']>;
  collectionId?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  isLiveAuction?: InputMaybe<Scalars['Boolean']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ItemOrder>;
  query?: InputMaybe<Scalars['String']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type QuerySingleItemArgs = {
  contractAddress: Scalars['String'];
  token: Scalars['Int'];
};

export type RangeDto = {
  name: Scalars['String'];
  value: Scalars['Float'];
  valueof: Scalars['Float'];
};

export type ResetPasswordInput = {
  code: Scalars['String'];
  email: Scalars['String'];
  password: Scalars['String'];
  passwordConfirm: Scalars['String'];
};

export type ResponseMessageWithStatusModel = {
  __typename?: 'ResponseMessageWithStatusModel';
  /** message */
  message: Scalars['String'];
  /** success */
  success: Scalars['Boolean'];
};

export type Role = {
  __typename?: 'Role';
  /** Identifies the date and time when the object was created. */
  createdAt: Scalars['Date'];
  id: Scalars['Int'];
  name: Scalars['String'];
  permissions?: Maybe<Scalars['String']>;
  /** Identifies the date and time when the object was last updated. */
  updatedAt: Scalars['Date'];
};

export type RoleOrder = {
  direction: Scalars['String'];
  field: Scalars['String'];
};

export type Section = {
  __typename?: 'Section';
  description?: Maybe<Scalars['String']>;
  title: Scalars['String'];
};

export type Sections = {
  __typename?: 'Sections';
  category: Section;
};

export type SettingsDto = {
  __typename?: 'SettingsDto';
  cur: Scalars['String'];
  sections: Sections;
};

export type SignupInput = {
  email: Scalars['String'];
  name: Scalars['String'];
  password: Scalars['String'];
  phone?: InputMaybe<Scalars['String']>;
  username: Scalars['String'];
};

export type Staff = {
  __typename?: 'Staff';
  avatar?: Maybe<Scalars['String']>;
  /** Identifies the date and time when the object was created. */
  createdAt: Scalars['Date'];
  email: Scalars['String'];
  id: Scalars['Int'];
  name: Scalars['String'];
  phone?: Maybe<Scalars['String']>;
  role?: Maybe<Role>;
  roleId?: Maybe<Scalars['Int']>;
  /** Identifies the date and time when the object was last updated. */
  updatedAt: Scalars['Date'];
  username: Scalars['String'];
};

export type StaffCreateInput = {
  avatar?: InputMaybe<Scalars['String']>;
  email: Scalars['String'];
  name: Scalars['String'];
  password: Scalars['String'];
  phone?: InputMaybe<Scalars['String']>;
  roleId?: InputMaybe<Scalars['Int']>;
  username: Scalars['String'];
};

export type StaffLoginInput = {
  password: Scalars['String'];
  username: Scalars['String'];
};

export type StaffUpdateInput = {
  avatar?: InputMaybe<Scalars['String']>;
  email?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  password?: InputMaybe<Scalars['String']>;
  phone?: InputMaybe<Scalars['String']>;
  roleId?: InputMaybe<Scalars['Int']>;
  username?: InputMaybe<Scalars['String']>;
};

export type Token = {
  __typename?: 'Token';
  /** JWT access token */
  accessToken: Scalars['String'];
  /** JWT expiration time */
  expireAt: Scalars['Date'];
  /** JWT refresh token */
  refreshToken: Scalars['String'];
};

export type UpdatePasswordInput = {
  oldPassword: Scalars['String'];
  password: Scalars['String'];
  passwordConfirm: Scalars['String'];
};

export type UpdateProfileInput = {
  author_content?: InputMaybe<Scalars['String']>;
  avatar?: InputMaybe<Scalars['String']>;
  email: Scalars['String'];
  name?: InputMaybe<Scalars['String']>;
  phone?: InputMaybe<Scalars['String']>;
  resetCode?: InputMaybe<Scalars['String']>;
  username: Scalars['String'];
};

export type User = {
  __typename?: 'User';
  author_content?: Maybe<Scalars['String']>;
  avatar?: Maybe<Scalars['String']>;
  collections?: Maybe<Array<Collection>>;
  /** Identifies the date and time when the object was created. */
  createdAt: Scalars['Date'];
  createdItems?: Maybe<Array<Item>>;
  email: Scalars['String'];
  emailVerifiedAt?: Maybe<Scalars['Date']>;
  id: Scalars['Int'];
  isEmailVerified: Scalars['Boolean'];
  name: Scalars['String'];
  ownedItems?: Maybe<Array<Item>>;
  phone?: Maybe<Scalars['String']>;
  resetCode?: Maybe<Scalars['String']>;
  /** Identifies the date and time when the object was last updated. */
  updatedAt: Scalars['Date'];
  username: Scalars['String'];
  walletAddress?: Maybe<Scalars['String']>;
};

export type UserConnection = {
  __typename?: 'UserConnection';
  edges?: Maybe<Array<UserEdge>>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type UserEdge = {
  __typename?: 'UserEdge';
  cursor: Scalars['String'];
  node: User;
};

export type CollectionInput = {
  coverImage?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  image?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  royalties: Scalars['Float'];
};

export type RoleInput = {
  name: Scalars['String'];
  permissions?: InputMaybe<Scalars['String']>;
};

export type SendForgetPasswordMailMutationVariables = Exact<{
  email: Scalars['String'];
}>;


export type SendForgetPasswordMailMutation = { __typename?: 'Mutation', sendForgetPasswordMail: { __typename?: 'ResponseMessageWithStatusModel', success: boolean, message: string } };

export type StaffLoginMutationVariables = Exact<{
  username: Scalars['String'];
  password: Scalars['String'];
}>;


export type StaffLoginMutation = { __typename?: 'Mutation', StaffLogin: { __typename?: 'Token', accessToken: string, refreshToken: string, expireAt: any } };

export type UpdateStaffPasswordMutationVariables = Exact<{
  oldPassword: Scalars['String'];
  password: Scalars['String'];
  passwordConfirm: Scalars['String'];
}>;


export type UpdateStaffPasswordMutation = { __typename?: 'Mutation', updateStaffPassword: { __typename?: 'ResponseMessageWithStatusModel', success: boolean, message: string } };

export type UpdateStaffProfileMutationVariables = Exact<{
  name: Scalars['String'];
  email: Scalars['String'];
  username: Scalars['String'];
  avatar: Scalars['String'];
  phone: Scalars['String'];
}>;


export type UpdateStaffProfileMutation = { __typename?: 'Mutation', updateStaffProfile: { __typename?: 'ResponseMessageWithStatusModel', success: boolean, message: string } };

export type UploadFileMutationVariables = Exact<{
  file: Scalars['Upload'];
}>;


export type UploadFileMutation = { __typename?: 'Mutation', uploadFile: { __typename?: 'FileObject', name: string, type: string, url: string } };

export type CreateRoleMutationVariables = Exact<{
  name: Scalars['String'];
  permissions: Scalars['String'];
}>;


export type CreateRoleMutation = { __typename?: 'Mutation', createRole: { __typename?: 'ResponseMessageWithStatusModel', success: boolean, message: string } };

export type DeleteRoleMutationVariables = Exact<{
  id: Scalars['Int'];
}>;


export type DeleteRoleMutation = { __typename?: 'Mutation', deleteRole: { __typename?: 'ResponseMessageWithStatusModel', success: boolean, message: string } };

export type DeleteStaffMutationVariables = Exact<{
  id: Scalars['Int'];
}>;


export type DeleteStaffMutation = { __typename?: 'Mutation', deleteStaff: { __typename?: 'ResponseMessageWithStatusModel', success: boolean, message: string } };

export type UpdateRoleMutationVariables = Exact<{
  id: Scalars['Int'];
  name: Scalars['String'];
  permissions: Scalars['String'];
}>;


export type UpdateRoleMutation = { __typename?: 'Mutation', updateRole: { __typename?: 'ResponseMessageWithStatusModel', success: boolean, message: string } };

export type UpdateStaffMutationVariables = Exact<{
  id: Scalars['Int'];
  name: Scalars['String'];
  email: Scalars['String'];
  username: Scalars['String'];
  avatar?: InputMaybe<Scalars['String']>;
  phone?: InputMaybe<Scalars['String']>;
  roleId?: InputMaybe<Scalars['Int']>;
}>;


export type UpdateStaffMutation = { __typename?: 'Mutation', updateStaff: { __typename?: 'ResponseMessageWithStatusModel', success: boolean, message: string } };

export type GetRoleQueryVariables = Exact<{
  id: Scalars['Int'];
}>;


export type GetRoleQuery = { __typename?: 'Query', getRole: { __typename?: 'Role', id: number, name: string, permissions?: string | null | undefined, createdAt: any } };

export type GetRolesQueryVariables = Exact<{
  query?: InputMaybe<Scalars['String']>;
  orderBy?: InputMaybe<RoleOrder>;
}>;


export type GetRolesQuery = { __typename?: 'Query', getRoles: Array<{ __typename?: 'Role', id: number, name: string, permissions?: string | null | undefined, createdAt: any }> };

export type GetStaffQueryVariables = Exact<{
  id: Scalars['Int'];
}>;


export type GetStaffQuery = { __typename?: 'Query', getStaff: { __typename?: 'Staff', id: number, name: string, username: string, email: string, phone?: string | null | undefined, avatar?: string | null | undefined, roleId?: number | null | undefined, role?: { __typename?: 'Role', id: number, name: string } | null | undefined } };

export type GetStaffListsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetStaffListsQuery = { __typename?: 'Query', getStaffLists: Array<{ __typename?: 'Staff', id: number, name: string, email: string, phone?: string | null | undefined, avatar?: string | null | undefined, roleId?: number | null | undefined, role?: { __typename?: 'Role', id: number, name: string, permissions?: string | null | undefined } | null | undefined }> };

export type ListFileQueryVariables = Exact<{ [key: string]: never; }>;


export type ListFileQuery = { __typename?: 'Query', listFile: Array<{ __typename?: 'FileObject', name: string, type: string, url: string }> };

export type StaffQueryVariables = Exact<{ [key: string]: never; }>;


export type StaffQuery = { __typename?: 'Query', staff: { __typename?: 'Staff', id: number, name: string, username: string, email: string, avatar?: string | null | undefined, phone?: string | null | undefined, roleId?: number | null | undefined } };


export const SendForgetPasswordMailDocument = `
    mutation SendForgetPasswordMail($email: String!) {
  sendForgetPasswordMail(email: $email) {
    success
    message
  }
}
    `;
export const useSendForgetPasswordMailMutation = <
      TError = unknown,
      TContext = unknown
    >(options?: UseMutationOptions<SendForgetPasswordMailMutation, TError, SendForgetPasswordMailMutationVariables, TContext>) =>
    useMutation<SendForgetPasswordMailMutation, TError, SendForgetPasswordMailMutationVariables, TContext>(
      'SendForgetPasswordMail',
      (variables?: SendForgetPasswordMailMutationVariables) => graphqlFetcher<SendForgetPasswordMailMutation, SendForgetPasswordMailMutationVariables>(SendForgetPasswordMailDocument, variables)(),
      options
    );
export const StaffLoginDocument = `
    mutation StaffLogin($username: String!, $password: String!) {
  StaffLogin(data: {username: $username, password: $password}) {
    accessToken
    refreshToken
    expireAt
  }
}
    `;
export const useStaffLoginMutation = <
      TError = unknown,
      TContext = unknown
    >(options?: UseMutationOptions<StaffLoginMutation, TError, StaffLoginMutationVariables, TContext>) =>
    useMutation<StaffLoginMutation, TError, StaffLoginMutationVariables, TContext>(
      'StaffLogin',
      (variables?: StaffLoginMutationVariables) => graphqlFetcher<StaffLoginMutation, StaffLoginMutationVariables>(StaffLoginDocument, variables)(),
      options
    );
export const UpdateStaffPasswordDocument = `
    mutation UpdateStaffPassword($oldPassword: String!, $password: String!, $passwordConfirm: String!) {
  updateStaffPassword(
    data: {oldPassword: $oldPassword, password: $password, passwordConfirm: $passwordConfirm}
  ) {
    success
    message
  }
}
    `;
export const useUpdateStaffPasswordMutation = <
      TError = unknown,
      TContext = unknown
    >(options?: UseMutationOptions<UpdateStaffPasswordMutation, TError, UpdateStaffPasswordMutationVariables, TContext>) =>
    useMutation<UpdateStaffPasswordMutation, TError, UpdateStaffPasswordMutationVariables, TContext>(
      'UpdateStaffPassword',
      (variables?: UpdateStaffPasswordMutationVariables) => graphqlFetcher<UpdateStaffPasswordMutation, UpdateStaffPasswordMutationVariables>(UpdateStaffPasswordDocument, variables)(),
      options
    );
export const UpdateStaffProfileDocument = `
    mutation UpdateStaffProfile($name: String!, $email: String!, $username: String!, $avatar: String!, $phone: String!) {
  updateStaffProfile(
    data: {email: $email, name: $name, username: $username, phone: $phone, avatar: $avatar}
  ) {
    success
    message
  }
}
    `;
export const useUpdateStaffProfileMutation = <
      TError = unknown,
      TContext = unknown
    >(options?: UseMutationOptions<UpdateStaffProfileMutation, TError, UpdateStaffProfileMutationVariables, TContext>) =>
    useMutation<UpdateStaffProfileMutation, TError, UpdateStaffProfileMutationVariables, TContext>(
      'UpdateStaffProfile',
      (variables?: UpdateStaffProfileMutationVariables) => graphqlFetcher<UpdateStaffProfileMutation, UpdateStaffProfileMutationVariables>(UpdateStaffProfileDocument, variables)(),
      options
    );
export const UploadFileDocument = `
    mutation UploadFile($file: Upload!) {
  uploadFile(file: $file) {
    name
    type
    url
  }
}
    `;
export const useUploadFileMutation = <
      TError = unknown,
      TContext = unknown
    >(options?: UseMutationOptions<UploadFileMutation, TError, UploadFileMutationVariables, TContext>) =>
    useMutation<UploadFileMutation, TError, UploadFileMutationVariables, TContext>(
      'UploadFile',
      (variables?: UploadFileMutationVariables) => graphqlFetcher<UploadFileMutation, UploadFileMutationVariables>(UploadFileDocument, variables)(),
      options
    );
export const CreateRoleDocument = `
    mutation createRole($name: String!, $permissions: String!) {
  createRole(data: {name: $name, permissions: $permissions}) {
    success
    message
  }
}
    `;
export const useCreateRoleMutation = <
      TError = unknown,
      TContext = unknown
    >(options?: UseMutationOptions<CreateRoleMutation, TError, CreateRoleMutationVariables, TContext>) =>
    useMutation<CreateRoleMutation, TError, CreateRoleMutationVariables, TContext>(
      'createRole',
      (variables?: CreateRoleMutationVariables) => graphqlFetcher<CreateRoleMutation, CreateRoleMutationVariables>(CreateRoleDocument, variables)(),
      options
    );
export const DeleteRoleDocument = `
    mutation deleteRole($id: Int!) {
  deleteRole(id: $id) {
    success
    message
  }
}
    `;
export const useDeleteRoleMutation = <
      TError = unknown,
      TContext = unknown
    >(options?: UseMutationOptions<DeleteRoleMutation, TError, DeleteRoleMutationVariables, TContext>) =>
    useMutation<DeleteRoleMutation, TError, DeleteRoleMutationVariables, TContext>(
      'deleteRole',
      (variables?: DeleteRoleMutationVariables) => graphqlFetcher<DeleteRoleMutation, DeleteRoleMutationVariables>(DeleteRoleDocument, variables)(),
      options
    );
export const DeleteStaffDocument = `
    mutation deleteStaff($id: Int!) {
  deleteStaff(id: $id) {
    success
    message
  }
}
    `;
export const useDeleteStaffMutation = <
      TError = unknown,
      TContext = unknown
    >(options?: UseMutationOptions<DeleteStaffMutation, TError, DeleteStaffMutationVariables, TContext>) =>
    useMutation<DeleteStaffMutation, TError, DeleteStaffMutationVariables, TContext>(
      'deleteStaff',
      (variables?: DeleteStaffMutationVariables) => graphqlFetcher<DeleteStaffMutation, DeleteStaffMutationVariables>(DeleteStaffDocument, variables)(),
      options
    );
export const UpdateRoleDocument = `
    mutation updateRole($id: Int!, $name: String!, $permissions: String!) {
  updateRole(id: $id, data: {name: $name, permissions: $permissions}) {
    success
    message
  }
}
    `;
export const useUpdateRoleMutation = <
      TError = unknown,
      TContext = unknown
    >(options?: UseMutationOptions<UpdateRoleMutation, TError, UpdateRoleMutationVariables, TContext>) =>
    useMutation<UpdateRoleMutation, TError, UpdateRoleMutationVariables, TContext>(
      'updateRole',
      (variables?: UpdateRoleMutationVariables) => graphqlFetcher<UpdateRoleMutation, UpdateRoleMutationVariables>(UpdateRoleDocument, variables)(),
      options
    );
export const UpdateStaffDocument = `
    mutation updateStaff($id: Int!, $name: String!, $email: String!, $username: String!, $avatar: String, $phone: String, $roleId: Int) {
  updateStaff(
    id: $id
    data: {email: $email, name: $name, username: $username, phone: $phone, avatar: $avatar, roleId: $roleId}
  ) {
    success
    message
  }
}
    `;
export const useUpdateStaffMutation = <
      TError = unknown,
      TContext = unknown
    >(options?: UseMutationOptions<UpdateStaffMutation, TError, UpdateStaffMutationVariables, TContext>) =>
    useMutation<UpdateStaffMutation, TError, UpdateStaffMutationVariables, TContext>(
      'updateStaff',
      (variables?: UpdateStaffMutationVariables) => graphqlFetcher<UpdateStaffMutation, UpdateStaffMutationVariables>(UpdateStaffDocument, variables)(),
      options
    );
export const GetRoleDocument = `
    query getRole($id: Int!) {
  getRole(id: $id) {
    id
    name
    permissions
    createdAt
  }
}
    `;
export const useGetRoleQuery = <
      TData = GetRoleQuery,
      TError = unknown
    >(
      variables: GetRoleQueryVariables,
      options?: UseQueryOptions<GetRoleQuery, TError, TData>
    ) =>
    useQuery<GetRoleQuery, TError, TData>(
      ['getRole', variables],
      graphqlFetcher<GetRoleQuery, GetRoleQueryVariables>(GetRoleDocument, variables),
      options
    );
export const useInfiniteGetRoleQuery = <
      TData = GetRoleQuery,
      TError = unknown
    >(
      variables: GetRoleQueryVariables,
      options?: UseInfiniteQueryOptions<GetRoleQuery, TError, TData>
    ) =>
    useInfiniteQuery<GetRoleQuery, TError, TData>(
      ['getRole.infinite', variables],
      (metaData) => graphqlFetcher<GetRoleQuery, GetRoleQueryVariables>(GetRoleDocument, {...variables, ...(metaData.pageParam ?? {})})(),
      options
    );

export const GetRolesDocument = `
    query getRoles($query: String, $orderBy: RoleOrder) {
  getRoles(query: $query, orderBy: $orderBy) {
    id
    name
    permissions
    createdAt
  }
}
    `;
export const useGetRolesQuery = <
      TData = GetRolesQuery,
      TError = unknown
    >(
      variables?: GetRolesQueryVariables,
      options?: UseQueryOptions<GetRolesQuery, TError, TData>
    ) =>
    useQuery<GetRolesQuery, TError, TData>(
      variables === undefined ? ['getRoles'] : ['getRoles', variables],
      graphqlFetcher<GetRolesQuery, GetRolesQueryVariables>(GetRolesDocument, variables),
      options
    );
export const useInfiniteGetRolesQuery = <
      TData = GetRolesQuery,
      TError = unknown
    >(
      variables?: GetRolesQueryVariables,
      options?: UseInfiniteQueryOptions<GetRolesQuery, TError, TData>
    ) =>
    useInfiniteQuery<GetRolesQuery, TError, TData>(
      variables === undefined ? ['getRoles.infinite'] : ['getRoles.infinite', variables],
      (metaData) => graphqlFetcher<GetRolesQuery, GetRolesQueryVariables>(GetRolesDocument, {...variables, ...(metaData.pageParam ?? {})})(),
      options
    );

export const GetStaffDocument = `
    query getStaff($id: Int!) {
  getStaff(id: $id) {
    id
    name
    username
    email
    phone
    avatar
    roleId
    role {
      id
      name
    }
  }
}
    `;
export const useGetStaffQuery = <
      TData = GetStaffQuery,
      TError = unknown
    >(
      variables: GetStaffQueryVariables,
      options?: UseQueryOptions<GetStaffQuery, TError, TData>
    ) =>
    useQuery<GetStaffQuery, TError, TData>(
      ['getStaff', variables],
      graphqlFetcher<GetStaffQuery, GetStaffQueryVariables>(GetStaffDocument, variables),
      options
    );
export const useInfiniteGetStaffQuery = <
      TData = GetStaffQuery,
      TError = unknown
    >(
      variables: GetStaffQueryVariables,
      options?: UseInfiniteQueryOptions<GetStaffQuery, TError, TData>
    ) =>
    useInfiniteQuery<GetStaffQuery, TError, TData>(
      ['getStaff.infinite', variables],
      (metaData) => graphqlFetcher<GetStaffQuery, GetStaffQueryVariables>(GetStaffDocument, {...variables, ...(metaData.pageParam ?? {})})(),
      options
    );

export const GetStaffListsDocument = `
    query getStaffLists {
  getStaffLists {
    id
    name
    email
    phone
    avatar
    roleId
    role {
      id
      name
      permissions
    }
  }
}
    `;
export const useGetStaffListsQuery = <
      TData = GetStaffListsQuery,
      TError = unknown
    >(
      variables?: GetStaffListsQueryVariables,
      options?: UseQueryOptions<GetStaffListsQuery, TError, TData>
    ) =>
    useQuery<GetStaffListsQuery, TError, TData>(
      variables === undefined ? ['getStaffLists'] : ['getStaffLists', variables],
      graphqlFetcher<GetStaffListsQuery, GetStaffListsQueryVariables>(GetStaffListsDocument, variables),
      options
    );
export const useInfiniteGetStaffListsQuery = <
      TData = GetStaffListsQuery,
      TError = unknown
    >(
      variables?: GetStaffListsQueryVariables,
      options?: UseInfiniteQueryOptions<GetStaffListsQuery, TError, TData>
    ) =>
    useInfiniteQuery<GetStaffListsQuery, TError, TData>(
      variables === undefined ? ['getStaffLists.infinite'] : ['getStaffLists.infinite', variables],
      (metaData) => graphqlFetcher<GetStaffListsQuery, GetStaffListsQueryVariables>(GetStaffListsDocument, {...variables, ...(metaData.pageParam ?? {})})(),
      options
    );

export const ListFileDocument = `
    query ListFile {
  listFile {
    name
    type
    url
  }
}
    `;
export const useListFileQuery = <
      TData = ListFileQuery,
      TError = unknown
    >(
      variables?: ListFileQueryVariables,
      options?: UseQueryOptions<ListFileQuery, TError, TData>
    ) =>
    useQuery<ListFileQuery, TError, TData>(
      variables === undefined ? ['ListFile'] : ['ListFile', variables],
      graphqlFetcher<ListFileQuery, ListFileQueryVariables>(ListFileDocument, variables),
      options
    );
export const useInfiniteListFileQuery = <
      TData = ListFileQuery,
      TError = unknown
    >(
      variables?: ListFileQueryVariables,
      options?: UseInfiniteQueryOptions<ListFileQuery, TError, TData>
    ) =>
    useInfiniteQuery<ListFileQuery, TError, TData>(
      variables === undefined ? ['ListFile.infinite'] : ['ListFile.infinite', variables],
      (metaData) => graphqlFetcher<ListFileQuery, ListFileQueryVariables>(ListFileDocument, {...variables, ...(metaData.pageParam ?? {})})(),
      options
    );

export const StaffDocument = `
    query Staff {
  staff {
    id
    name
    username
    email
    avatar
    phone
    roleId
  }
}
    `;
export const useStaffQuery = <
      TData = StaffQuery,
      TError = unknown
    >(
      variables?: StaffQueryVariables,
      options?: UseQueryOptions<StaffQuery, TError, TData>
    ) =>
    useQuery<StaffQuery, TError, TData>(
      variables === undefined ? ['Staff'] : ['Staff', variables],
      graphqlFetcher<StaffQuery, StaffQueryVariables>(StaffDocument, variables),
      options
    );
export const useInfiniteStaffQuery = <
      TData = StaffQuery,
      TError = unknown
    >(
      variables?: StaffQueryVariables,
      options?: UseInfiniteQueryOptions<StaffQuery, TError, TData>
    ) =>
    useInfiniteQuery<StaffQuery, TError, TData>(
      variables === undefined ? ['Staff.infinite'] : ['Staff.infinite', variables],
      (metaData) => graphqlFetcher<StaffQuery, StaffQueryVariables>(StaffDocument, {...variables, ...(metaData.pageParam ?? {})})(),
      options
    );
