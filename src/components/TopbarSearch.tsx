import React from 'react';
import Select from 'react-select';

const TopbarSearch = () => {
    return (
        <Select
            placeholder={'Search...'}
            maxMenuHeight={350}
            isSearchable
            isClearable
            name="search-app"
            className="app-search dropdown"
            classNamePrefix="react-select"
        />
    );
};

export default TopbarSearch;
