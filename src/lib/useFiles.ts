import axios from 'axios';
import { useMutation, useQuery } from 'react-query';

const getAllFiles = async () => {
    const { data } = await axios({
        url: `http://localhost:4000/graphql`,
        method: 'post',
        data: {
            query: `
     {
      allUsers { id name }
     }
     `,
        },
    });
    return data;
};

const uploadProductFiles = async (file:any) => {
    const data = await axios({
        url: `http://localhost:4000/graphql`,
        method: 'post',
        data: {
            query: `
      mutation Mutation($file:Upload!){
          uploadFile(file: $file){
            name
            type
            url
          }
        }
     `,
        },
    });

    return data;
};

export const useFiles = () => {
    return useQuery('files', getAllFiles);
};

export const useUploadFile = () => {
    return useMutation(uploadProductFiles);
};
