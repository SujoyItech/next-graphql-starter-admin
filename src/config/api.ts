import {APIConfigType} from "./config";
const APIConfig: APIConfigType = {
    endpoint: process.env.API_URL || 'http://localhost:3000/graphql',
    fetchParams: {
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        }
    }
}
export default APIConfig;