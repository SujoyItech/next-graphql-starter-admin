export type APIConfigType = {
  endpoint: string;
  fetchParams: {
    headers: {}
  };
}