import React, {useEffect, useState} from 'react';
import Link from 'next/link';
import classNames from 'classnames';

// actions
import { showRightSidebar, changeSidebarType } from '../src/redux/actions';

// store

//constants
import { LayoutTypes, SideBarTypes } from '../src/constants/layout';

import Image from "next/image";

import LanguageDropdown from '../src/components/LanguageDropdown';
import NotificationDropdown from '../src/components/NotificationDropdown';
import ProfileDropdown from '../src/components/ProfileDropdown';

import profilePic from '../src/assets/images/users/user-1.jpg';
import avatar4 from '../src/assets/images/users/user-4.jpg';
import logoSm from '../src/assets/images/logo-sm.png';
import logoDark from '../src/assets/images/logo-dark.png';
import logoLight from '../src/assets/images/logo-light.png';
import {useApp} from "../src/contexts/AppProvider";
import {useAuth} from "../src/contexts/AuthProvider";
import {useStaffQuery} from "../src/graphql/generated";
import {useToasts} from "react-toast-notifications";
import {AddToastAlert, ShowToastAlert} from "../src/Services/ToastAlertService";
import Router, {useRouter} from "next/router";

export interface NotificationItem {
    id: number;
    text: string;
    subText: string;
    icon?: string;
    avatar?: any;
    bgColor?: string;
}

// get the notifications
const Notifications: NotificationItem[] = [
    {
        id: 1,
        text: 'Cristina Pride',
        subText: 'Hi, How are you? What about our next meeting',
        avatar: profilePic,
    },
    {
        id: 2,
        text: 'Caleb Flakelar commented on Admin',
        subText: '1 min ago',
        icon: 'mdi mdi-comment-account-outline',
        bgColor: 'primary',
    },
    {
        id: 3,
        text: 'Karen Robinson',
        subText: 'Wow ! this admin looks good and awesome design',
        avatar: avatar4,
    },
    {
        id: 4,
        text: 'New user registered.',
        subText: '5 hours ago',
        icon: 'mdi mdi-account-plus',
        bgColor: 'warning',
    },
    {
        id: 5,
        text: 'Caleb Flakelar commented on Admin',
        subText: '1 min ago',
        icon: 'mdi mdi-comment-account-outline',
        bgColor: 'info',
    },
    {
        id: 6,
        text: 'Carlos Crouch liked Admin',
        subText: '13 days ago',
        icon: 'mdi mdi-heart',
        bgColor: 'secondary',
    },
];

// get the profilemenu
const ProfileMenus = [
    {
        key: 'my-account',
        label: 'My Account',
        icon: 'fe-user',
        redirectTo: '/',
    },
    {
        key: 'settings',
        label: 'Settings',
        icon: 'fe-settings',
        redirectTo: '/',
    },
    {
        key: 'lock-screen',
        label: 'Lock Screen',
        icon: 'fe-lock',
        redirectTo: '/auth/lock-screen',
    },
    {
        key: 'logout',
        label: 'Logout',
        icon: 'fe-log-out',
        redirectTo: '/auth/logout',
    },
];

// dummy search results
const SearchResults = [
    {
        id: 1,
        title: 'Analytics Report',
        icon: 'uil-notes',
        redirectTo: '/',
    },
    {
        id: 2,
        title: 'How can I help you?',
        icon: 'uil-life-ring',
        redirectTo: '/',
    },
    {
        id: 3,
        icon: 'uil-cog',
        title: 'User profile settings',
        redirectTo: '/',
    },
];

const otherOptions = [
    {
        id: 1,
        label: 'New Projects',
        icon: 'fe-briefcase',
    },
    {
        id: 2,
        label: 'Create Users',
        icon: 'fe-user',
    },
    {
        id: 3,
        label: 'Revenue Report',
        icon: 'fe-bar-chart-line-',
    },
    {
        id: 4,
        label: 'Settings',
        icon: 'fe-settings',
    },
    {
        id: 4,
        label: 'Help & Support',
        icon: 'fe-headphones',
    },
];

// get mega-menu options
const MegaMenuOptions = [
    {
        id: 1,
        title: 'UI Components',
        menuItems: [
            'Widgets',
            'Nestable List',
            'Range Sliders',
            'Masonry Items',
            'Sweet Alerts',
            'Treeview Page',
            'Tour Page',
        ],
    },
    {
        id: 2,
        title: 'Applications',
        menuItems: [
            'eCommerce Pages',
            'CRM Pages',
            'Email',
            'Calendar',
            'Team Contacts',
            'Task Board',
            'Email Templates',
        ],
    },
    {
        id: 3,
        title: 'Extra Pages',
        menuItems: [
            'Left Sidebar with User',
            'Menu Collapsed',
            'Small Left Sidebar',
            'New Header Style',
            'Search Result',
            'Gallery Pages',
            'Maintenance & Coming Soon',
        ],
    },
];

interface TopbarProps {
    hideLogo?: boolean;
    navCssClasses?: string;
    openLeftMenuCallBack?: () => void;
    topbarDark?: boolean;
}


const Topbar = ({ hideLogo, navCssClasses, openLeftMenuCallBack, topbarDark }: TopbarProps) => {
    const{setCondensed,isOpenRightSideBar,setOpenRightSideBar} = useApp();
    const [isopen, setIsopen] = useState<boolean>(false);
    const [leftSideBarType, setLeftSideBarType] = useState<string>('default');
    const navbarCssClasses: string = navCssClasses || '';
    const containerCssClasses: string = !hideLogo ? 'container-fluid' : '';

    const [me,setMe] = useState<any>(undefined);
    const {signOut} = useAuth();
    const {data,isSuccess} = useStaffQuery()
    const { addToast } = useToasts();

    const router = useRouter();

    useEffect(()=>{
        if (isSuccess){
            setMe(data?data.staff : undefined);
        }
    },[data])

    const logout = async (e:any)=>{
        try {
            e.preventDefault();
            await signOut();
            AddToastAlert("Logout successful!","success");
            router.push('/');
        }catch (error){
            addToast("Logout failed!",{appearance: "error"})
        }
    }

    /**
     * Toggle the leftmenu when having mobile screen
     */
    const handleLeftMenuCallBack = () => {
        setIsopen(!isopen);
        if (openLeftMenuCallBack) openLeftMenuCallBack();
    };


    const toggleLeftSidebarWidth = () =>{
        if (leftSideBarType === 'default' || leftSideBarType === 'compact'){
            setLeftSideBarType(SideBarTypes.LEFT_SIDEBAR_TYPE_CONDENSED)
            setCondensed(true);
        }
        if (leftSideBarType === 'condensed'){
            setLeftSideBarType(SideBarTypes.LEFT_SIDEBAR_TYPE_DEFAULT);
            setCondensed(false);
        }
    };

    return (
        <>
            <div className={`navbar-custom ${navbarCssClasses}`}>
                <div className={containerCssClasses}>
                    {!hideLogo && (
                        <div className="logo-box">
                            <Link href="/">
                                <a className="logo logo-dark text-center">
                                   <span className="logo-sm">
                                        <Image src={logoSm} alt="" height="22" width="22"/>
                                    </span>
                                    <span className="logo-lg">
                                        <Image
                                            src={logoDark}
                                            alt=""
                                            height="30"
                                            width="100"
                                        />
                                    </span>
                                </a>
                            </Link>
                            <Link href="/">
                                <a className="logo logo-light text-center">
                                    <span className="logo-sm">
                                        <Image src={logoSm} alt="" height="22" width="22" />
                                    </span>
                                    <span className="logo-lg">
                                        <Image
                                            src={logoLight}
                                            alt=""
                                            height="30"
                                            width="100"
                                        />
                                    </span>
                                </a>
                            </Link>
                        </div>
                    )}

                    <ul className="list-unstyled topnav-menu float-end mb-0">
                        <li className="dropdown d-none d-lg-inline-block topbar-dropdown">
                            <LanguageDropdown />
                        </li>
                        <li className="dropdown notification-list topbar-dropdown">
                            <NotificationDropdown notifications={Notifications} />
                        </li>
                        <li className="dropdown notification-list topbar-dropdown">
                            <ProfileDropdown
                                profilePic={profilePic}
                                menuItems={ProfileMenus}
                                username={'Geneva'}
                                userTitle={'Founder'}
                                logout = {logout}
                            />
                        </li>
                    </ul>

                    <ul className="list-unstyled topnav-menu topnav-menu-left m-0">
                        <li>
                            <button
                                className="button-menu-mobile waves-effect waves-light d-none d-lg-block"
                                onClick={toggleLeftSidebarWidth}
                            >
                                <i className="fe-menu"></i>
                            </button>
                        </li>

                        <li>
                            <button
                                className="button-menu-mobile open-left d-lg-none d-bolck waves-effect waves-light"
                                onClick={handleLeftMenuCallBack}
                            >
                                <i className="fe-menu" />
                            </button>
                        </li>

                        {/* Mobile menu toggle (Horizontal Layout) */}
                        <li>
                            <Link
                                href="#"
                            >
                                <a className={classNames('navbar-toggle nav-link open')} onClick={handleLeftMenuCallBack}>
                                    <div className="lines">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </a>

                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
        </>
    );
};

export default Topbar;
