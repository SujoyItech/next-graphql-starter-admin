import React, {ReactElement, useEffect} from 'react';
import { Container, Row, Col, Card } from 'react-bootstrap';
import Link from 'next/link';

interface AccountLayoutProps {
    helpText?: string;
    bottomLinks?: any;
    isCombineForm?: boolean;
    children?: any;
}

const AuthLayout = (page: ReactElement) => {

    return (
        <>
            <div className="account-pages mt-5 mb-5">
                <Container>
                    <Row className="justify-content-center">
                        <Col md={8} lg={6} xl={ 4}>
                            {page}
                        </Col>
                    </Row>
                </Container>
            </div>

            <footer className="footer footer-alt">
                2015 - {new Date().getFullYear()} &copy; UBold theme by{' '}
                <Link href="#" >
                    <a className="text-white-50"> Coderthemes</a>
                </Link>
            </footer>
        </>
    );
};

export default AuthLayout;
