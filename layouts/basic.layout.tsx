// @ts-ignore
import React, {ReactElement, Suspense, useEffect, useState} from "react";
import {Container} from "react-bootstrap";
import MainSection from "../sections/MainSection";



const BasicLayout = (page: ReactElement) => {
    return (
        <>
            <MainSection>
                {page}
            </MainSection>
        </>
    )
}

export default BasicLayout;

