

// @ts-ignore
import React, {ReactElement, Suspense, useEffect, useState} from "react";
import {Container} from "react-bootstrap";

const Topbar = React.lazy(() => import('./Topbar'));
const LeftSidebar = React.lazy(() => import('./LeftSidebar'));
const Footer = React.lazy(() => import('./Footer'));


const loading = () => <div className="text-center"></div>;

const TestLayout = (page: ReactElement) => {
    return (
        <>
            <div id="wrapper">
                <Suspense fallback={loading()}>
                    <Topbar
                        navCssClasses="topnav-navbar topnav-navbar-dark"
                        topbarDark={true}
                        hideLogo={false}
                    />
                </Suspense>
                <Suspense fallback={loading()}>
                    <LeftSidebar/>
                </Suspense>

                <div className="content-page">
                    <div className="content">
                        <Container fluid>
                            <Suspense fallback={loading()}>{page}</Suspense>
                        </Container>
                    </div>

                    <Suspense fallback={loading()}>
                        <Footer />
                    </Suspense>
                </div>
            </div>
        </>
    )
}

export default TestLayout;
