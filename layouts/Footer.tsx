import React from 'react';
import { Row, Col } from 'react-bootstrap';
import Link  from 'next/link';

const Footer = () => {
    const currentYear = new Date().getFullYear();

    return (
        <>
            <footer className="footer">
                <div className="container-fluid">
                    <Row>
                        <Col md={6}>
                            {currentYear} &copy; UBold theme by <Link href="#">Coderthemes</Link>
                        </Col>

                        <Col md={6}>
                            <div className="text-md-end footer-links d-none d-sm-block">
                                <Link href="#">About Us</Link>
                                <Link href="#">Help</Link>
                                <Link href="#">Contact Us</Link>
                            </div>
                        </Col>
                    </Row>
                </div>
            </footer>
        </>
    );
};

export default Footer;
