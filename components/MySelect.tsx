import {Field, useField} from "formik";
import {Form} from "react-bootstrap";
const MySelect = ({children, label, ...props } : any) => {
    const [field, meta] = useField(props);
    return (
        <Form.Group className="mb-2">
            { label && <label htmlFor={props.id || props.name}>{label}</label> }
            <Field as="select" {...field} {...props}>
                {children}
            </Field>
            {meta.touched && meta.error ? (
                <p className="text-danger">{meta.error}</p>
            ) : null}
        </Form.Group>
    );
};
export default MySelect;
