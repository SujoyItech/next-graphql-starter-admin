import Props from './ComponentInterface'
export default function Form({children, className, ...rest} : Props){
    return  (
        <form className={`${className}`} {...rest}>
            {children}
        </form>
    )
}