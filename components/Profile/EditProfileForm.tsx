import {Form, Formik} from "formik";
import * as Yup from "yup";
import MyTextInput from "../MyTextInput";
import React, {useState} from "react";
import MediaModalBody from "../Media/MediaModalBody";
import MyModal from "../MyModal";
import {useMedia} from "../../src/contexts/MediaProvider";
import avatarImage from "../../public/assets/images/dashbord/profile-image.png";
import Image from "next/image";
import {useUpdateStaffProfileMutation} from "../../src/graphql/generated";
import {useToasts} from "react-toast-notifications";
import Router from "next/router";

export default function EditProfileForm({user}:any){
    const [modal, setModal] = useState(false);

    const [avatar, setAvatar] = useState<string>(user.avatar ? user.avatar : "")

    const { addToast } = useToasts();

    const {getMediaImage,setMediaImage} = useMedia();

    const updateProfile = useUpdateStaffProfileMutation();

    const Toggle = () => setModal(!modal);
    const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

    const selectImage = () => {
        Toggle();
        setAvatar('');
    }
    const closeModal = ()=>{
        const selectedImage = getMediaImage();
        setAvatar(selectedImage)
        setMediaImage('');
        Toggle();
    }

    return(
        <>
            <Formik
                initialValues={{
                    name: user.name,
                    username: user.username,
                    email: user.email,
                    phone: user.phone,
                    avatar: user.avatar
                }}
                validationSchema={Yup.object({
                    name: Yup.string()
                        .max(15, 'Must be 15 characters or less'),
                    email: Yup.string()
                        .email('Invalid email address'),
                    phone: Yup.string().matches(phoneRegExp, 'Phone number is not valid'),
                })}
                onSubmit= { async (values, { setSubmitting }) => {
                    try {
                        values.avatar = avatar;
                        const response = await updateProfile.mutateAsync(values);
                        if (response.updateStaffProfile){
                            if (response.updateStaffProfile.success === true){
                                localStorage.setItem('response-type','success');
                                localStorage.setItem('response-message',response.updateStaffProfile.message);
                                Router.push('/profile');
                            }else{
                                addToast(response.updateStaffProfile.message, { appearance: "error" });
                            }
                        }else {
                            addToast('Something went wrong!', { appearance: "error" });
                        }
                    }catch (error){
                        addToast('Something went wrong!', { appearance: "error" });
                    }
                }}
            >
                <Form>

                    <div className="row">
                        <div className="col-md-4 col-lg-3">
                            <div className="profile-page-left">
                                <h2 className="dashbord-section-title">My Profile</h2>
                                <div className="profile-image">
                                    <Image src={ avatar ? avatar : avatarImage} alt="live-action-image" onError={()=>avatarImage} layout="fill"/>
                                    <div className="uplode-file">
                                        <label htmlFor="fileuplode" className="file-uplode-btn"><i
                                            className="fas fa-camera"></i></label>
                                        <button className="form-control-file" type="button" id="fileuplode" onClick={selectImage}>Upload</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-8 col-lg-9">
                            <div className="profile-page-right">
                                <h2 className="dashbord-section-title">General Information</h2>
                                <div className="section-inner-wrap">
                                    <h3 className="section-inner-title">Profile Information</h3>
                                    <div className="dashbord-form">
                                        <MyTextInput label="Name" name="name" className="form-control"  id="name" type="text"
                                                     placeholder="Enter Name"/>
                                        <MyTextInput label="UserName" name="username" className="form-control"  id="username" type="text" readOnly={true}/>
                                        <MyTextInput label="Email" name="email" className="form-control" type="email" id="email" readOnly={true}
                                                     placeholder="Enter Email"/>
                                        <MyTextInput label="Phone" name="phone" className="form-control" type="text" id="phone"
                                                     placeholder="Enter Phone"/>
                                    </div>
                                </div>
                                <div className="sectoin-bottom text-center">
                                    <button type="submit" className="primary-btn">Update</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </Form>

            </Formik>
            <MyModal show={modal} size="xl" title="Media Modal" close={closeModal}>
                <MediaModalBody/>
            </MyModal>
        </>
    )
}