import {useEffect, useState} from "react";
import {Button, Modal} from "react-bootstrap";

export default function MyModal({show,close,title,children,...props} : any) {
    return (
        <>
            {
                show ?
                    <Modal
                        show={show}
                        backdrop="static"
                        keyboard={false}
                        {...props}
                    >
                        <div className="modal-content text-dark">
                            <div className="modal-header">
                                <h5 className="modal-title">{title}</h5>
                                <button type="button" onClick={()=>close()} className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div className="card">
                                    <div className="card-body">
                                        {children}
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-lg btn-dark" onClick={()=>close()}><i className="fas fa-save"></i> Save</button>
                            </div>
                        </div>
                    </Modal> : null
            }

        </>
    );
}