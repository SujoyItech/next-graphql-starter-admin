import Props from "./ComponentInterface";

export default function FormButton({children, ...rest} : Props){
    return (
        <button {...rest}>
            {children}
        </button>
    )
}