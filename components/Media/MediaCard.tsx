import React, {useState} from "react";
import {useMedia} from "../../src/contexts/MediaProvider"
export default function MediaCard({name,url}:any){
    const {setMediaImage} = useMedia();
    const [selectedImage, setSelectedImage] = useState<string | null>(null);
    const onImageSelected = (imageUrl : string)=>{
        setMediaImage(imageUrl);
        setSelectedImage(null);
        setSelectedImage(imageUrl)
    }
    return(
        <div className="col-lg-3 col-md-6">
            <div className="single-live-action">
                <div className="live-action-thumbnail">
                    <img src={url} alt="live-action-image"/>
                    <a onClick={()=>onImageSelected(url)} className="primary-btn">{selectedImage && selectedImage === url ? 'Selected' : 'Select'}</a>
                </div>
            </div>
        </div>
    )
}