import * as Yup from "yup";
import {useRouter} from "next/router";
import {Form, Formik} from "formik";
import MyTextInput from "../MyTextInput";
import React, {useState} from "react";
import {useToasts} from "react-toast-notifications";
import {useCreateRoleMutation} from "../../src/graphql/generated";
import {Card, Row,Col} from "react-bootstrap";

import {Success} from "../../src/Services/SwalAlertService"

export default function ApplicationSettingForm(){
    const { addToast } = useToasts();
    const [successMessage,setSuccessMessage] = useState('');
    const createRole = useCreateRoleMutation();
    const router = useRouter();
    return(
        <>
            <Formik
                initialValues={{
                    application_title: '',
                    contact_email: '',
                    contact_phone: '',
                    address: '',
                    adminWalletAddress: '',
                    adminCommission: 0.0,
                }}
                validationSchema={Yup.object({
                    application_title: Yup.string().max(50, 'Must be 50 characters or less'),
                    contact_email: Yup.string().email('Invalid email address!'),
                    contact_phone: Yup.string().max(15,'Invalid phone!'),
                    address: Yup.string(),
                    adminWalletAddress: Yup.string(),
                    adminCommission: Yup.number().max(100,'Commission must be less than or equal 100').min(0,'Commission must be greater than or equal')
                })}
                onSubmit= { async (values, { setSubmitting }) => {
                    try {
                        const response = await createRole.mutateAsync(values);
                        if (response.createRole){
                            if (response.createRole.success === true){
                                setSuccessMessage(response.createRole.message);
                            }else{
                                addToast(response.createRole.message, { appearance: "error" });
                            }
                        }else {
                            addToast('Something went wrong!', { appearance: "error" });
                        }
                    }catch (error:any){
                        addToast(error[0].message, { appearance: "error" });
                    }
                }}
            >
                <Form>
                    <Card>
                        <Card.Body>
                            <Row>
                                <Col md={12} lg={12}>
                                    <h4 className="header-title">Application Settings</h4>
                                    <p className="sub-header">
                                        Create staff roles and permission from here..
                                    </p>
                                </Col>
                            </Row>
                            <Row>
                                <Col lg={6}>
                                    <MyTextInput label="Application Title" name="application_title" className="form-control"  id="application_title" type="text"
                                                 placeholder="Enter application title"/>
                                </Col>
                                <Col lg={6}>
                                    <MyTextInput label="Contact Email" name="contact_email" className="form-control"  id="contact_email" type="email"
                                                 placeholder="Enter contact email"/>
                                </Col>
                                <Col lg={6}>
                                    <MyTextInput label="Contact Phone" name="contact_phone" className="form-control"  id="contact_phone" type="text"
                                                 placeholder="Enter contact phone"/>
                                </Col>
                                <Col lg={6}>
                                    <MyTextInput label="Address" name="address" className="form-control"  id="address" type="text"
                                                 placeholder="Enter address"/>
                                </Col>
                                <Col lg={6}>
                                    <MyTextInput label="Wallet address" name="adminWalletAddress" className="form-control"  id="adminWalletAddress" type="text"
                                                 placeholder="Enter wallet address"/>
                                </Col>
                                <Col lg={6}>
                                    <MyTextInput label="Admin commission ( % )" name="adminCommission" className="form-control"  id="adminCommission" type="number" max={100} min={0}
                                                 placeholder="Enter commission"/>
                                </Col>
                                <Col lg={6}>
                                    <button type="submit" className="btn btn-dark"><i className="fas fa-save"></i> Create</button>
                                </Col>
                            </Row>
                        </Card.Body>
                    </Card>
                </Form>
            </Formik>
            {successMessage && Success(successMessage,()=>{
                router.push('/role');
            })}
        </>
    )
}
