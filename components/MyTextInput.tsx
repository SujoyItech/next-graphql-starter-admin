import {useField} from "formik";
import {Form} from "react-bootstrap";
import React from "react";
const MyTextInput = ({label,...props } : any) => {
    // useField() returns [formik.getFieldProps(), formik.getFieldMeta()]
    // which we can spread on <input>. We can use field meta to show an error
    // message if the field is invalid and it has been touched (i.e. visited)
    const [field,meta] = useField(props);
    return (
        <Form.Group className="mb-2">
            {label ? <Form.Label>{label}</Form.Label> : null}
            <input className="form-control mb-1" {...field} {...props} />
            {meta.touched && meta.error ? (
                <p className="text-danger">{meta.error}</p>
            ) : null}
        </Form.Group>
    );
};
export default MyTextInput;
