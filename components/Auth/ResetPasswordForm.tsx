import {Formik,Form} from "formik";
import * as Yup from "yup";
import Router from "next/router";
import MyTextInput from "../../components/MyTextInput";
import {useChangePasswordMutation} from "../../src/graphql/generated";
import {useToasts} from "react-toast-notifications";
import {AddToastAlert} from "../../src/Services/ToastAlertService";
export default function ResetPasswordForm(){
    const { addToast } = useToasts();
    const changePassword = useChangePasswordMutation();
    return (
        <>
            <Formik
                initialValues={{
                    code: '',
                    email: '',
                    password: '',
                    passwordConfirm: ''
                }}
                validationSchema={Yup.object({
                    code: Yup.string().required('Verification code is required!'),
                    email: Yup.string().email('Invalid email!').required('Email is required!'),
                    password: Yup.string().min(6).required('Password is required'),
                    passwordConfirm: Yup.string()
                        .oneOf([Yup.ref('password'), null], 'Passwords must match'),
                })}
                onSubmit= { async (values, { setSubmitting }) => {
                    try {
                        const response = await changePassword.mutateAsync(values);
                        if (response.changePassword.success === true){
                            setSubmitting(false);
                            AddToastAlert(response.changePassword.message,"success");
                            Router.push('/login')
                        }else {
                            addToast(response.changePassword.message, { appearance: "error" });
                        }
                    }catch (error){
                        addToast("Something went wrong!", { appearance: "error" });
                    }
                }}
            >
                <Form>
                    <MyTextInput name="email" className="form-control" type="email" placeholder="Enter email" />
                    <MyTextInput name="code" className="form-control" type="text" placeholder="Enter code" />
                    <MyTextInput name="password" className="form-control" type="password" placeholder="Enter password" />
                    <MyTextInput name="passwordConfirm" className="form-control" type="password" placeholder="Confirm password" />
                    <button type="submit" className="register-btn mb-5">Submit</button>
                </Form>
            </Formik>

        </>
    )
}