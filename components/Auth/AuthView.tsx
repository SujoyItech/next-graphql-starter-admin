import React, {ReactNode, useEffect} from "react";
type Props = {
    children: ReactNode;
    helpText: string;
};
import Image from "next/image";
import {ShowToastAlert} from "../../src/Services/ToastAlertService";
import {Card} from "react-bootstrap";
import Link from "next/link";
import LogoDark from "../../src/assets/images/logo-dark.png";
import LogoLight from "../../src/assets/images/logo-light.png";
export default function AuthView( { children,helpText } : Props ){
    useEffect(() => {
        if (document.body) document.body.classList.add('authentication-bg', 'authentication-bg-pattern');
    }, []);
    ShowToastAlert();
    return (
        <>
            <Card className="bg-pattern">
                <Card.Body className="p-4">
                    <div className="text-center w-75 m-auto">
                        <div className="auth-logo">
                            <Link href="/" >
                                <a className="logo logo-dark text-center">
                                     <span className="logo-lg">
                                        <Image src={LogoDark} alt="" height="22" width="100" />
                                    </span>
                                </a>
                            </Link>

                            <Link href="/">
                                <a className="logo logo-light text-center">
                                    <span className="logo-lg">
                                        <Image src={LogoLight} alt="" height="22" width="100" />
                                    </span>
                                </a>
                            </Link>
                        </div>
                        {helpText && <p className="text-muted mb-2 mt-2">{helpText}</p>}
                    </div>
                    {children}
                </Card.Body>
            </Card>
        </>
    )

}
