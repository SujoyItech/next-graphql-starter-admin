import {Formik,Form} from "formik";
import * as Yup from "yup";
import Router from "next/router";
import MyTextInput from "../../components/MyTextInput";
import {useSendForgetPasswordMailMutation} from "../../src/graphql/generated";
import {useToasts} from "react-toast-notifications";
import {AddToastAlert} from "../../src/Services/ToastAlertService";
export default function ForgetPasswordForm(){
    const { addToast } = useToasts();
    const sendForgetPasswordMail = useSendForgetPasswordMailMutation();
    return (
        <>
            <Formik
                initialValues={{
                    email: '',
                }}
                validationSchema={Yup.object({
                    email: Yup.string().email('Invalid email')
                        .required('Email or Username is required')
                })}
                onSubmit= { async (values, { setSubmitting }) => {
                    try {
                        const response = await sendForgetPasswordMail.mutateAsync(values);
                        if (response.sendForgetPasswordMail.success === true){
                            setSubmitting(false);
                            AddToastAlert(response.sendForgetPasswordMail.message,"success");
                            Router.push('/reset-password')
                        }else {
                            addToast(response.sendForgetPasswordMail.message, { appearance: "error" });
                        }
                    }catch (error){
                        addToast("Something went wrong!", { appearance: "error" });
                    }

                }}
            >
                <Form>
                    <MyTextInput name="email" className="form-control" type="email"
                                 placeholder="Enter email" />
                    <button type="submit" className="register-btn mb-5">Submit</button>
                </Form>
            </Formik>

        </>
    )
}