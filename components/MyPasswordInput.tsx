import {useField} from "formik";
import {Form, InputGroup} from "react-bootstrap";
import React, {useState} from "react";
import classNames from "classnames";
const MyPasswordInput = ({label,...props } : any) => {
    const [showPassword, setShowPassword] = useState<boolean>(false);
    const [field,meta] = useField(props);
    return (
        <Form.Group className="mb-2">
            {label ? <Form.Label>{label}</Form.Label> : null}
            <InputGroup className="mb-1">
                <input type={showPassword ? 'text' : 'password'} className="form-control mb-0" {...field} {...props} />
                <div className={classNames('input-group-text', 'input-group-password', {
                    'show-password': showPassword,
                })}
                     data-password={showPassword ? 'true' : 'false'}
                >
                    <span
                        className="password-eye"
                        onClick={() => {
                            setShowPassword(!showPassword);
                        }}
                    ></span>
                </div>

            </InputGroup>

            {meta.touched && meta.error ? (
                <p className="text-danger">{meta.error}</p>
            ) : null}

        </Form.Group>

    );
};
export default MyPasswordInput;
