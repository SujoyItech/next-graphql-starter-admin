import {Spinner} from "react-bootstrap";

export default function Loader(){
    return(
        <div className="animation-box">
            <Spinner animation="grow" role="status">
                <span className="visually-hidden"></span>
            </Spinner>
        </div>
    )
}