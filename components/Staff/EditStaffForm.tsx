import * as Yup from "yup";
import {useRouter} from "next/router";
import {Form, Formik} from "formik";
import MyTextInput from "../MyTextInput";
import React, {useState} from "react";
import {useToasts} from "react-toast-notifications";
import {useGetRolesQuery, useUpdateStaffMutation} from "../../src/graphql/generated";
import {Card, Col, Row} from "react-bootstrap";
import {Success} from "../../src/Services/SwalAlertService";
import Link from "next/link";
import MySelect from "../MySelect";

export default function EditStaffForm({staff} : any ){
    const router = useRouter();
    const { addToast } = useToasts();
    const [successMessage,setSuccessMessage] = useState('');
    const [sort, setSort] = useState({
        field: 'id',
        direction: 'desc'
    });
    const filterData = { orderBy: sort}
    const [roles,setRoles] = useState<any>();
    const updateStaff = useUpdateStaffMutation();
    useGetRolesQuery({
       ...filterData
    },{
        onSuccess({getRoles}){
            setRoles(getRoles);
        }
    });
    console.log(staff);
    return(
        <>
            <Formik
                initialValues={{
                    id: staff.id,
                    name: staff.name,
                    username: staff.username,
                    email: staff.email,
                    phone: staff.phone,
                    roleId: staff.roleId,
                }}
                validationSchema={Yup.object({
                    name: Yup.string().max(50, 'Must be 50 characters or less').required('Name is required!'),
                    username: Yup.string().max(20, 'Must be 20 characters or less').required('Username is required!'),
                    email: Yup.string().email('Invalid email!').required('Email is required!'),
                    phone: Yup.string().nullable().max(20,'Invalid phone!'),
                    avatar: Yup.string().nullable(),
                    roleId: Yup.number().required('Role is required!'),
                })}
                onSubmit= { async (values, { setSubmitting }) => {
                    try {
                        values.roleId = parseInt(values.roleId);
                        const response = await updateStaff.mutateAsync(values);
                        if (response.updateStaff){
                            if (response.updateStaff.success === true){
                                setSuccessMessage(response.updateStaff.message);
                            }else{
                                addToast(response.updateStaff.message, { appearance: "error" });
                            }
                        }else {
                            addToast('Something went wrong!', { appearance: "error" });
                        }
                    }catch (error:any){
                        addToast(error[0].message, { appearance: "error" });
                    }
                }}
            >
                <Form>
                    <Card>
                        <Card.Body>
                            <Row>
                                <Col md={6} lg={6}>
                                    <h4 className="header-title">Update staff</h4>
                                    <p className="sub-header">
                                        Update staff from here..
                                    </p>
                                </Col>
                                <Col md={6} lg={6}>
                                    <div className="d-flex align-items-center justify-content-end">
                                        <Link href='/staff'><button className="btn btn-dark"><i className="fas fa-list"></i> Staff Lists</button></Link>
                                    </div>
                                </Col>
                            </Row>
                            <Row>
                                <Col lg={6}>
                                    <MyTextInput label="Name" name="name" className="form-control"  id="name" type="text"
                                                 placeholder="Enter Name"/>
                                    <MyTextInput label="Username" name="username" className="form-control"  id="username" type="text"/>
                                    <MyTextInput label="Email" name="email" className="form-control"  id="email" type="email"/>
                                    <MyTextInput label="Phone" name="phone" className="form-control"  id="phone" type="text"/>
                                    <MySelect label="Role" className="form-control mb-1" name="roleId" id="roleId">
                                        <option value="" disabled={true}>Select Role</option>
                                        {
                                            roles && roles.length > 0 && roles.map((role : any)=>{
                                                return(
                                                    <option value={role.id} key={role.id}>{role.name}</option>
                                                )
                                            })
                                        }
                                    </MySelect>
                                    <button type="submit" className="btn btn-dark"><i className="fas fa-save"></i> Update</button>
                                </Col>
                            </Row>
                        </Card.Body>
                    </Card>
                </Form>
            </Formik>
            {successMessage && Success(successMessage,()=>{
                router.push('/staff');
            })}
        </>
    )
}
