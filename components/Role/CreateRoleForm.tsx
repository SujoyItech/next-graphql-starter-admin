import * as Yup from "yup";
import {useRouter} from "next/router";
import {Form, Formik} from "formik";
import MyTextInput from "../MyTextInput";
import React, {useState} from "react";
import {useToasts} from "react-toast-notifications";
import {useCreateRoleMutation} from "../../src/graphql/generated";
import {Card, Row,Col} from "react-bootstrap";

import {Success} from "../../src/Services/SwalAlertService"
import Link from "next/link";

export default function CreateRoleForm(){
    const { addToast } = useToasts();
    const [successMessage,setSuccessMessage] = useState('');
    const createRole = useCreateRoleMutation();
    const router = useRouter();
    return(
        <>
            <Formik
                initialValues={{
                    name: '',
                    permissions: '',
                }}
                validationSchema={Yup.object({
                    name: Yup.string().required('Name field is required!')
                        .max(50, 'Must be 50 characters or less'),
                    permission: Yup.string(),
                })}
                onSubmit= { async (values, { setSubmitting }) => {
                    try {
                        const response = await createRole.mutateAsync(values);
                        if (response.createRole){
                            if (response.createRole.success === true){
                                setSuccessMessage(response.createRole.message);
                            }else{
                                addToast(response.createRole.message, { appearance: "error" });
                            }
                        }else {
                            addToast('Something went wrong!', { appearance: "error" });
                        }
                    }catch (error:any){
                        addToast(error[0].message, { appearance: "error" });
                    }
                }}
            >
                <Form>
                    <Card>
                        <Card.Body>
                            <Row>
                                <Col md={6} lg={6}>
                                    <h4 className="header-title">Create new roles</h4>
                                    <p className="sub-header">
                                        Create staff roles and permission from here..
                                    </p>
                                </Col>
                                <Col md={6} lg={6}>
                                    <div className="d-flex align-items-center justify-content-end">
                                        <Link href='/role'><button className="btn btn-dark"><i className="fas fa-list"></i> Role Lists</button></Link>
                                    </div>
                                </Col>
                            </Row>
                            <Row>
                                <Col lg={6}>
                                    <MyTextInput label="Name" name="name" className="form-control"  id="name" type="text"
                                                 placeholder="Enter Name"/>
                                    <MyTextInput label="Permissions" name="permissions" className="form-control"  id="permissions" type="text"/>
                                    <button type="submit" className="btn btn-dark"><i className="fas fa-save"></i> Create</button>
                                </Col>
                            </Row>
                        </Card.Body>
                    </Card>
                </Form>
            </Formik>
            {successMessage && Success(successMessage,()=>{
                router.push('/role');
            })}
        </>
    )
}
