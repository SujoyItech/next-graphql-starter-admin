import * as Yup from "yup";
import {useRouter} from "next/router";
import {Form, Formik} from "formik";
import MyTextInput from "../MyTextInput";
import React, {useState} from "react";
import {useToasts} from "react-toast-notifications";
import {useUpdateRoleMutation} from "../../src/graphql/generated";
import {Card, Col, Row} from "react-bootstrap";
import {Success} from "../../src/Services/SwalAlertService";
import Link from "next/link";

export default function EditRoleForm({role} : any ){
    const router = useRouter();
    const { addToast } = useToasts();
    const [successMessage,setSuccessMessage] = useState('');
    const updateRole = useUpdateRoleMutation();
    return(
        <>
            <Formik
                initialValues={{
                    id: role.id,
                    name: role.name,
                    permissions: role.permissions,
                }}
                validationSchema={Yup.object({
                    name: Yup.string()
                        .max(50, 'Must be 50 characters or less'),
                })}
                onSubmit= { async (values, { setSubmitting }) => {
                    try {
                        const response = await updateRole.mutateAsync(values);
                        if (response.updateRole){
                            if (response.updateRole.success === true){
                                setSuccessMessage(response.updateRole.message);
                            }else{
                                addToast(response.updateRole.message, { appearance: "error" });
                            }
                        }else {
                            addToast('Something went wrong!', { appearance: "error" });
                        }
                    }catch (error:any){
                        addToast(error[0].message, { appearance: "error" });
                    }
                }}
            >
                <Form>
                    <Card>
                        <Card.Body>
                            <Row>
                                <Col md={6} lg={6}>
                                    <h4 className="header-title">Update roles</h4>
                                    <p className="sub-header">
                                        Update staff roles and permission from here..
                                    </p>
                                </Col>
                                <Col md={6} lg={6}>
                                    <div className="d-flex align-items-center justify-content-end">
                                        <Link href='/role'><button className="btn btn-dark"><i className="fas fa-list"></i> Role Lists</button></Link>
                                    </div>
                                </Col>
                            </Row>
                            <Row>
                                <Col lg={6}>
                                    <MyTextInput label="Name" name="name" className="form-control"  id="name" type="text"
                                                 placeholder="Enter Name"/>
                                    <MyTextInput label="Permissions" name="permissions" className="form-control"  id="permissions" type="text"/>
                                    <button type="submit" className="btn btn-dark"><i className="fas fa-save"></i> Update</button>
                                </Col>
                            </Row>
                        </Card.Body>
                    </Card>
                </Form>
            </Formik>
            {successMessage && Success(successMessage,()=>{
                router.push('/role');
            })}
        </>
    )
}
