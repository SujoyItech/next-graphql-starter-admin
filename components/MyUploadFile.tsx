import {useField} from "formik";

export default function MyUploadFile({label,id,...props } : any){
    const [field,meta] = useField(props);
    return(
        <>
            <div className="form-group">
                <input type="file" id={id} {...field} {...props} />
                <label className="uplode-text" htmlFor={id}>{label}</label>
            </div>
            {meta.touched && meta.error ? (
                <div className="text-danger p-1">{meta.error}</div>
            ) : null}
        </>
    )
}