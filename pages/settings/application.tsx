import {NextPageWithLayout} from "../../src/types";
import BasicLayout from "../../layouts/basic.layout";
import PageTitle from "../../src/components/PageTitle";
import CreateRoleForm from "../../components/Role/CreateRoleForm";
import React from "react";
import ApplicationSettingForm from "../../components/Settings/ApplicationSettingForm";

const ApplicationSetting: NextPageWithLayout = () => {
    return (
        <>
            <PageTitle
                breadCrumbItems={[
                    { label: 'Settings', path: '/settings/application' },
                    { label: 'Application Settings', path: '/settings/application', active: true },
                ]}
                title={'Application Settings'}
            />
            <ApplicationSettingForm/>
        </>
    )
}
ApplicationSetting.getLayout = BasicLayout
export default ApplicationSetting;
