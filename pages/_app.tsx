import "../src/assets/scss/Saas.scss"

import {AppPropsWithLayout} from "../src/types";
import {QueryClient, QueryClientProvider} from "react-query";
import { ReactQueryDevtools } from 'react-query/devtools'


const queryClient = new QueryClient();

import AuthProvider from "../src/contexts/AuthProvider";

import { ToastProvider} from 'react-toast-notifications';
import MediaProvider from "../src/contexts/MediaProvider";
import RouteGuard from "../src/Services/RouteGuard";
import AppProvider from "../src/contexts/AppProvider";

function NftApp({ Component, pageProps }: AppPropsWithLayout) {
  const getLayout = Component.getLayout ?? ((page) => page)

  return (
      <AppProvider>
          <QueryClientProvider client={queryClient}>
              <ToastProvider autoDismiss={true} placement="bottom-right">
                  <MediaProvider>
                      <AuthProvider>
                          <RouteGuard>
                              <>
                                  {getLayout(<Component {...pageProps} />)}
                                  <ReactQueryDevtools initialIsOpen={false} />
                              </>
                          </RouteGuard>
                      </AuthProvider>
                  </MediaProvider>
              </ToastProvider>
          </QueryClientProvider>
      </AppProvider>
  )
}

export default NftApp;
