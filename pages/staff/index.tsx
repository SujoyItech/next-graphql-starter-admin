import {NextPageWithLayout} from "../../src/types";
import BasicLayout from "../../layouts/basic.layout";
import {useEffect} from "react";
import StaffSection from "../../sections/staff/StaffSection";

const Staff: NextPageWithLayout = () => {

    return (
        <>
            <StaffSection/>
        </>
    )
}
Staff.getLayout = BasicLayout
export default Staff;
