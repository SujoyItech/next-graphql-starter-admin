import {NextPageWithLayout} from "../../../src/types";
import BasicLayout from "../../../layouts/basic.layout";
import React, {useState} from "react";
import {useGetStaffQuery} from "../../../src/graphql/generated";
import PageTitle from "../../../src/components/PageTitle";
import EditStaffForm from "../../../components/Staff/EditStaffForm";

const EditStaff: NextPageWithLayout = ({data}:any) => {
    const[staff, setStaff] = useState<any>();
    useGetStaffQuery({id : parseInt(data.edit_id) },{
        onSuccess({getStaff}){
            setStaff(getStaff);
        }
    });
    return (
        <>
            <PageTitle
                breadCrumbItems={[
                    { label: 'Staff Management', path: '/staff' },
                    { label: 'Edit Staff', path: '/staff/edit/'+data.edit_id, active: true },
                ]}
                title={'Staff Update'}
            />
            {staff && <EditStaffForm staff={staff}/>}
        </>
    )
}

export async function getServerSideProps(context : any) {
    const edit_id = context.params.edit_id // Get ID from slug `/book/1`
    const data = {
        edit_id : edit_id
    }
    // Rest of `getServerSideProps` code
    return { props: { data } }
}

EditStaff.getLayout = BasicLayout
export default EditStaff;

