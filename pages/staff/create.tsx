import {NextPageWithLayout} from "../../src/types";
import BasicLayout from "../../layouts/basic.layout";
import React, {useState} from "react";
import CreateRoleForm from "../../components/Role/CreateRoleForm";
import PageTitle from "../../src/components/PageTitle";

const CreateRole: NextPageWithLayout = () => {
    return (
        <>
            <PageTitle
                breadCrumbItems={[
                    { label: 'Role & Permissions', path: '/role' },
                    { label: 'Create Role', path: '/role/create', active: true },
                ]}
                title={'Roles & Permission Create'}
            />
            <CreateRoleForm/>
        </>
    )
}
CreateRole.getLayout = BasicLayout
export default CreateRole;

