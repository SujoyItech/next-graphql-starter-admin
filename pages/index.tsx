import {NextPageWithLayout} from "../src/types";
import AuthLayout from "../layouts/auth.layout";
import AuthView from "../components/Auth/AuthView";
import LoginForm from "../components/Auth/LoginForm";
import React from "react";
import {authPageRequireCheck} from "../middleware/authCheck";

const Login: NextPageWithLayout = ({data}:any) => {

    return (
        <>
            <AuthView helpText="Login here">
                <LoginForm redirectUrl = {data.redirect}/>
            </AuthView>
        </>
    )
}

export const getServerSideProps = async (context : any) => {
    const { redirect } = context.query;
    const redirectUrl = redirect ? redirect : '/dashboard';
    const checkRedirect = await authPageRequireCheck(context.req,context.res);
    if(checkRedirect){
        return {
            redirect: {
                permanent: true,
                destination: redirectUrl,
            }
        }
    }
    const data = {
        redirect: redirectUrl
    }
    return { props: {data}};
}

Login.getLayout = AuthLayout

export default Login
