import {NextPageWithLayout} from "../../src/types";
import BasicLayout from "../../layouts/basic.layout";
import EditProfileSection from "../../sections/profile/EditProfileSection";

const Edit: NextPageWithLayout = () => {
    return (
        <>
            <EditProfileSection/>
        </>
    )
}
Edit.getLayout = BasicLayout
export default Edit;