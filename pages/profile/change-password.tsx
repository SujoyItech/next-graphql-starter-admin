import {NextPageWithLayout} from "../../src/types";
import BasicLayout from "../../layouts/basic.layout";
import ChangePasswordSection from "../../sections/profile/ChangePasswordSection";

const ChangePassword: NextPageWithLayout = () => {
    return (
        <>
            <ChangePasswordSection/>
        </>
    )
}
ChangePassword.getLayout = BasicLayout
export default ChangePassword;
