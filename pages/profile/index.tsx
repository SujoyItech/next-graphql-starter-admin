import {NextPageWithLayout} from "../../src/types";
import UserProfileLayout from "../../layouts/basic.layout";
import ProfileSection from "../../sections/profile/ProfileSection";
import {useStaffQuery} from "../../src/graphql/generated";
import {useEffect, useState} from "react";
import Loader from "../../components/Loader";

const Profile: NextPageWithLayout = () => {
    const [me,setMe] = useState<any>(undefined);
    const {data,isSuccess} = useStaffQuery()
    useEffect(()=>{
        if (isSuccess){
            setMe(data?data.staff : undefined);
        }
    },[data])
    return (
        <>
            me ? <ProfileSection user={me}/> : <Loader/>
        </>
    )
}
Profile.getLayout = UserProfileLayout
export default Profile;