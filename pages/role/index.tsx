import {NextPageWithLayout} from "../../src/types";
import BasicLayout from "../../layouts/basic.layout";
import RoleSection from "../../sections/role/RoleSection";

const Role: NextPageWithLayout = () => {
    return (
        <>
            <RoleSection/>
        </>
    )
}
Role.getLayout = BasicLayout
export default Role;
