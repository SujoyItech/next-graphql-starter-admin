import {NextPageWithLayout} from "../../../src/types";
import BasicLayout from "../../../layouts/basic.layout";
import React, {useState} from "react";
import {useGetRoleQuery} from "../../../src/graphql/generated";
import PageTitle from "../../../src/components/PageTitle";
import EditRoleForm from "../../../components/Role/EditRoleForm";

const EditRole: NextPageWithLayout = ({data}:any) => {
    const[role, setRole] = useState<any>();
    useGetRoleQuery({id : parseInt(data.edit_id) },{
        onSuccess({getRole}){
            setRole(getRole);
        }
    });
    return (
        <>
            <PageTitle
                breadCrumbItems={[
                    { label: 'Role & Permissions', path: '/role' },
                    { label: 'Edit Role', path: '/role/edit/'+data.edit_id, active: true },
                ]}
                title={'Roles & Permission Update'}
            />
            {role && <EditRoleForm role={role}/>}
        </>
    )
}

export async function getServerSideProps(context : any) {
    const edit_id = context.params.edit_id // Get ID from slug `/book/1`
    const data = {
        edit_id : edit_id
    }
    // Rest of `getServerSideProps` code
    return { props: { data } }
}

EditRole.getLayout = BasicLayout
export default EditRole;

