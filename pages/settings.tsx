import {NextPageWithLayout} from "../src/types";
import BasicLayout from "../layouts/basic.layout";
import SettingSection from "../sections/profile/SettingSection";

const Setting: NextPageWithLayout = () => {
    return (
        <>
            <SettingSection/>
        </>
    )
}
Setting.getLayout = BasicLayout
export default Setting;
