import {NextPageWithLayout} from "../src/types";
import BasicLayout from "../layouts/basic.layout";
import DashboardSection from "../sections/profile/DashboardSection";
import {useEffect} from "react";

const Dashboard: NextPageWithLayout = () => {

    return (
        <>
            <DashboardSection/>
        </>
    )
}
Dashboard.getLayout = BasicLayout
export default Dashboard;
