import React, { useState } from 'react';
import { Row, Col } from 'react-bootstrap';
import {NextPageWithLayout} from "../src/types";
import TestLayout from "../layouts/test.layout";

const Dashboard: NextPageWithLayout = () => {
    return (
        <Row>
            <Col>
                <div className="page-title-box">
                    <div className="page-title-right">
                        <form className="d-flex align-items-center mb-3">
                            <div className="input-group input-group-sm">
                                <h4>Hello world!</h4>
                            </div>
                            <button className="btn btn-blue btn-sm ms-2">
                                <i className="mdi mdi-autorenew"></i>
                            </button>
                            <button className="btn btn-blue btn-sm ms-1">
                                <i className="mdi mdi-filter-variant"></i>
                            </button>
                        </form>
                    </div>
                    <h4 className="page-title">Dashboard</h4>
                </div>
            </Col>
        </Row>
    )
}
Dashboard.getLayout = TestLayout
export default Dashboard;
